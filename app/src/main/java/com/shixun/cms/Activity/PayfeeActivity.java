package com.shixun.cms.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.HashMap;
import java.util.Map;

public class PayfeeActivity extends AppCompatActivity {

    private TextView userfee_id,userfee_username,userfee_age,userfee_phone;

    private TextView getfee_houseid,getfee_chargeid,getfee_managerid,getfee_money,getfee_date,getfee_situaton;

    private ImageView payfeeback;

    private Button payfee;

    private Handler handler=new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            switch (msg.what){
                case Config.STATUS_OK:
                    Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                    if(result.getErrorCode()==Config.STATUS_SUCCESS){

                        startActivity(new Intent(PayfeeActivity.this,SuccesspayfeeActivity.class));
                        finish();

                    }else{

                        Toast.makeText(PayfeeActivity.this,"付款失败",Toast.LENGTH_LONG).show();
                    }
                    break;

                default:
                    Toast.makeText(PayfeeActivity.this,"网络连接失败,付款失败",Toast.LENGTH_LONG).show();
                    break;

            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payfee);
//        AndroidBarUtils.setTranslucent(this);
        Intent intent = getIntent();
        int feeType = intent.getIntExtra("feeType", 0);


        initView();
        inEvent(feeType);

    }

    private void inEvent(int feeType) {
        if (feeType == Config.FEE_TYPE){
            payfee.setVisibility(View.GONE);
        }
        SharedPreferences sharedPreferences= User.loadLoginUserInfo(this);
        String user_id=sharedPreferences.getString("user_id","null");
        String user_name=sharedPreferences.getString("user_name","null");
        String user_phone=sharedPreferences.getString("user_tel","null");
        int user_age=sharedPreferences.getInt("age",0);
        String user_age_result= String.valueOf(user_age);

        String houserid=getIntent().getStringExtra("houseid");
        String managerid=getIntent().getStringExtra("managerid");
        String chargeid=getIntent().getStringExtra("listnumber");
        String date=getIntent().getStringExtra("date");
        int situation=getIntent().getIntExtra("situation",-1);
        String money=getIntent().getStringExtra("money");


        userfee_id.setText(user_id);
        userfee_phone.setText(user_phone);
        userfee_username.setText(user_name);
        userfee_age.setText(user_age_result);

        if(situation==0){
            getfee_situaton.setText("未缴费");
            getfee_situaton.setTextColor(ContextCompat.getColor(this, R.color.red));
        }else{

            getfee_situaton.setText("已缴费");
            getfee_situaton.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        getfee_money.setText(money);
        getfee_managerid.setText(managerid);
        getfee_houseid.setText(houserid);
        getfee_date.setText(date);
        getfee_chargeid.setText(chargeid);


        payfeeback.setOnClickListener(view -> finish());


        payfee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String,Object> parasm=new HashMap<>();
                parasm.put("uid",user_id);
                parasm.put("cid",chargeid);

                OkHttpUtil.post("http://118.89.234.99:8080/xms/updatecharge",parasm,handler);

            }
        });


    }

    private void initView() {

        userfee_id=findViewById(R.id.userfee_id);
        userfee_age=findViewById(R.id.userfee_age);
        userfee_username=findViewById(R.id.userfee_username);
        userfee_phone=findViewById(R.id.userfee_phone);

        getfee_chargeid=findViewById(R.id.getfee_chargeid);
        getfee_date=findViewById(R.id.getfee_date);
        getfee_houseid=findViewById(R.id.getfee_houseid);
        getfee_managerid=findViewById(R.id.getfee_managerid);
        getfee_money=findViewById(R.id.getfee_money);
        getfee_situaton=findViewById(R.id.getfee_situation);

        payfeeback=findViewById(R.id.payfee_back);

        payfee=findViewById(R.id.payfee);

    }
}
