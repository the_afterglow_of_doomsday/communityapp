package com.shixun.cms.Activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.Calendar;

/**
 * 报修界面
 */
public class RepairActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private LinearLayout ll_repaire_type, ll_reserve_time;
    private EditText edt_repaire_detail;
    private TextView tv_repaire_type_result, tv_repaire_time_result;
    private Button btn_repaire_submit;
    private ImageView btn_back;

    private String hopeTime;
    private AlertDialog alertDialog;
    private String currentTime;
    private String repaireType;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == Config.STATUS_OK) {
                Toast.makeText(RepairActivity.this, "提交成功", Toast.LENGTH_SHORT).show();
                RepairActivity.this.finish();
            }
            else Toast.makeText(RepairActivity.this, "Error Occurs", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        AndroidBarUtils.setTranslucent(this);
        setContentView(R.layout.activity_repaire);
        // 获取登陆的用户信息
        sharedPreferences = User.loadLoginUserInfo(this);
        initView();
        initAction();
    }

    private void initAction() {
        ll_reserve_time.setOnClickListener(v -> chooseTime());
        ll_repaire_type.setOnClickListener(v->chooseRepaireType());
        btn_repaire_submit.setOnClickListener(v->submitForm());
        btn_back.setOnClickListener(v->finish());
    }

    // 提交表单
    private void submitForm() {
        String detail = edt_repaire_detail.getText().toString();
        if (checkForm()){
            String url = Config.URL_REPAIRE+
                    "rid="+ initRepaireID() +
                    "&uid="+ sharedPreferences.getString("user_id","undefined") +
                    "&mid=admin" +
                    "&rtp="+ repaireType +
                    "&rptt="+ getCurrentDate() +
                    "&rprt="+ "" +
                    "&pri="+ "" +
                    "&rpc="+"" +
                    "&pdt="+"" +
                    "&hpt="+hopeTime +
                    "&rps=0" +
                    "&dtl="+detail;

            Log.d("Repaire", "submitForm: \n"+ url);
            OkHttpUtil.get(url, handler);
        }
    }

    // 验证
    private boolean checkForm() {
        boolean a = repaireType == null || "".equals(repaireType);
        boolean b = hopeTime == null || "".equals(hopeTime);
        return !a && !b;
    }

    private void chooseRepaireType() {
        final String[] items = {"水", "电", "燃气", "公共环境/设施","其他"};
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setTitle("请选择");
        alertBuilder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.e("CHOOSE_TYPE", items[i]);
                repaireType = items[i];
                tv_repaire_type_result.setText(repaireType);
            }
        });

        alertBuilder.setPositiveButton("确定", (dialogInterface, i) -> alertDialog.dismiss());

        alertBuilder.setNegativeButton("取消", (dialogInterface, i) -> alertDialog.dismiss());

        alertDialog = alertBuilder.create();
        alertDialog.show();

    }

    private void chooseTime() {
        Calendar calendar = Calendar.getInstance();
        //create a datePickerDialog and then shoe it on your screen
        new DatePickerDialog(RepairActivity.this,//binding the listener for your DatePickerDialog
                (view, year, month, dayOfMonth) -> {
                    month++;
                    Log.e("DatePicker","Year:" + year + " Month:" + month + " Day:" + dayOfMonth);
                    if (month>0 && month<10 && dayOfMonth>0&&dayOfMonth<10){
                        hopeTime= year +"-0"+ month +"-0"+dayOfMonth;
                        tv_repaire_time_result.setText(hopeTime);
                        return;
                    }
                    else if (month>0 && month<10){
                        hopeTime= year +"-0"+ month +"-"+dayOfMonth;
                    }
                    else if (dayOfMonth>0&&dayOfMonth<10){
                        hopeTime= year +"-"+ month +"-0"+dayOfMonth;
                    }
                    else  hopeTime= year +"-"+ month +"-"+dayOfMonth;
                    tv_repaire_time_result.setText(hopeTime);
                }
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void initView() {
        ll_repaire_type = findViewById(R.id.repaire_type);
        ll_reserve_time = findViewById(R.id.reserve_time);
        edt_repaire_detail = findViewById(R.id.repaire_detail);
        btn_repaire_submit = findViewById(R.id.repaire_submit);
        tv_repaire_type_result = findViewById(R.id.repaire_type_result);
        tv_repaire_time_result = findViewById(R.id.reserve_time_result);
        btn_back = findViewById(R.id.repaire_back);
    }

    // 生成ID
    String initRepaireID(){
        return "005"+ System.currentTimeMillis();
    }

    // 获取当前日期
    String getCurrentDate(){
        int y,m,d;
        Calendar cal=Calendar.getInstance();
        y=cal.get(Calendar.YEAR);
        m=cal.get(Calendar.MONTH);
        d=cal.get(Calendar.DATE);
        if (m > 0 && m < 10 && d < 10){
            return y +"-0"+ m +"-0"+d;
        }
        else if (m>0 && m<10){
            return y +"-0"+ m +"-"+d;
        }
        else if (d < 10){
            return y +"-"+ m +"-0"+d;
        }
        else return y +"-"+ m +"-"+d;
    }
}
