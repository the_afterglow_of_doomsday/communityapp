package com.shixun.cms.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.shixun.cms.Adapter.SectionsPagerAdapter;
import com.shixun.cms.Fragment.FragmentOne;
import com.shixun.cms.Fragment.FragmentThree;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    NoScrollViewPager viewPager;
    BottomNavigationView navView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        AndroidBarUtils.setTranslucent(this);
        navView = findViewById(R.id.nav_view);
        viewPager = findViewById(R.id.nav_view_pager);
        initViewPager();
        navView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.m_navigation_home:
                    viewPager.setCurrentItem(0);
                    break;
                case R.id.m_navigation_me:
                    viewPager.setCurrentItem(1);
                    break;
            }
            return true;
        });
    }

    private void initViewPager() {
        // 配置ViewPager 用于设置左右滑动的页面
        viewPager.setOffscreenPageLimit(2);
        List<Fragment> fragments = new ArrayList<>();

        fragments.add(new FragmentOne());
//        fragments.add(new FragmentTwo());
        fragments.add(new FragmentThree());

//       向View Pager内添加适配器 用于与数据的绑定
//       Set a PagerAdapter that will supply views for this pager as needed.
        viewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager(), fragments));
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(0);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position){
            case 0:
                navView.setSelectedItemId(R.id.m_navigation_home);
                break;
            case 1:
                navView.setSelectedItemId(R.id.m_navigation_me);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
