package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shixun.cms.Entity.Manager;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

public class ManagerSendNewAnnounceActivity extends AppCompatActivity {
    private SharedPreferences mInfo;
    private String mid;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == Config.STATUS_OK){
                Toast.makeText(ManagerSendNewAnnounceActivity.this, "发布成功",Toast.LENGTH_SHORT).show();
                finish();
            }
            else{
                Toast.makeText(ManagerSendNewAnnounceActivity.this, "发布失败",Toast.LENGTH_SHORT).show();
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_send_new_annouce);
//        AndroidBarUtils.setTranslucent(this);
        mInfo = Manager.loadLoginManagerInfo(this);

        mid = mInfo.getString("manager_id", "admin");

        ImageView back = findViewById(R.id.send_announce_back);
        back.setOnClickListener(view -> finish());

        Button submit = findViewById(R.id.send_announce_submit_1);

        submit.setOnClickListener(v->submitNewAnnouncement());

        // 显示aid
        TextView tvAID =  findViewById(R.id.send_announce_aid);
        tvAID.setText(getAID());
    }

    /**
     * 提交新的公告
     */
    private void submitNewAnnouncement() {
        EditText send_announce_title = findViewById(R.id.send_announce_title);
        String title = send_announce_title.getText().toString();
        if ("".equals(title)){
            Toast.makeText(this, "标题不可为空", Toast.LENGTH_SHORT).show();
            return;
        }

        EditText send_announce_content = findViewById(R.id.send_announce_content);
        String content = send_announce_content.getText().toString();

        long currentTimeMillis = System.currentTimeMillis();

        String url = Config.URL_NEW_ANNOUNCE + "aid=" + getAID()
                +"&mid=" + mid
                +"&tit=" + title
                +"&con=" + content
                +"&sdt=" + currentTimeMillis;
        Log.e("URL------------>", url);
        OkHttpUtil.get(url, handler);
    }

    private String getAID(){
        return "a" + System.currentTimeMillis();
    }
}
