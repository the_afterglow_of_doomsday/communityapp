package com.shixun.cms.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;

public class SupportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
//        AndroidBarUtils.setTranslucent(this);
        Intent intent = getIntent();
        boolean title = intent.getBooleanExtra("title", false);
        if (title){
            TextView titleNew = findViewById(R.id.shared_title);
            titleNew.setText("物业信息");
        }
        ImageView back = findViewById(R.id.support_back);
        back.setOnClickListener(v->finish());
    }
}
