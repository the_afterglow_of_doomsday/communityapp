package com.shixun.cms.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.shixun.cms.Entity.Manager;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.PreferenceUtil;

import java.util.Timer;
import java.util.TimerTask;


public class SplashActivity extends AppCompatActivity {
    String userName;
    String managerName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        AndroidBarUtils.setTranslucent(this);
        // 初始化数据库
        // 2020.6.18 @fang

        //尝试取出已经保存的用户信息
        SharedPreferences userInfo = User.loadLoginUserInfo(this);
        this.userName = userInfo.getString("user_name","undefined");

        //尝试取出已经保存的管理员信息
        SharedPreferences managerInfo = Manager.loadLoginManagerInfo(this);
        this.managerName = managerInfo.getString("manager_account","undefined");

        // 展示并开始倒计时
        TimerTask timeTask = new TimerTask() {
            @Override
            public void run() {
                // 如果userName不是undefined 而且 允许自动登录跳转到主界面
                if (PreferenceUtil.getBoolean(SplashActivity.this, Config.SETTTING_AUTO_LOGIN) &&
                        !userName.equals("undefined")) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
                // 是否为管理员登录
                if (PreferenceUtil.getBoolean(SplashActivity.this, Config.SETTTING_AUTO_LOGIN_MANAGER) &&
                        !managerName.equals("undefined")){
                    startActivity(new Intent(SplashActivity.this, ManagerMainActivity.class));
                    finish();
                }
                // 否测直接进入登录界面
                else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        Timer timer = new Timer();
        timer.schedule(timeTask, 1000);
    }
}
