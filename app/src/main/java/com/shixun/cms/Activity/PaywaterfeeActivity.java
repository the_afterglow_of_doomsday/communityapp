package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.HashMap;
import java.util.Map;

public class PaywaterfeeActivity extends AppCompatActivity {
    private TextView userwaterfee_id,userwaterfee_username,userwaterfee_age;

    private TextView getwaterfee_chargeid,getwaterfee_managerid,getwaterfee_money,getwaterfee_date,getwaterfee_situaton;

    private TextView getwaterfee_gas,getwaterfee_water,getwaterfee_eletricty;

    private ImageView paywaterfeeback;

    private Button paywaterfee;

    private Handler handler=new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            switch (msg.what){
                case Config.STATUS_OK:
                    Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                    if(result.getErrorCode()==Config.STATUS_SUCCESS){

                        startActivity(new Intent(PaywaterfeeActivity.this,SuccesspayfeeActivity.class));
                        finish();

                    }else{

                        Toast.makeText(PaywaterfeeActivity.this,"付款失败",Toast.LENGTH_LONG).show();
                    }
                    break;

                default:
                    Toast.makeText(PaywaterfeeActivity.this,"网络连接失败,付款失败",Toast.LENGTH_LONG).show();
                    break;

            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paywaterfee);

        initView();
        inEvent();

    }

    private void inEvent() {

        SharedPreferences sharedPreferences= User.loadLoginUserInfo(this);
        String user_id=sharedPreferences.getString("user_id","null");
        String user_name=sharedPreferences.getString("user_name","null");
        int user_age=sharedPreferences.getInt("age",0);
        String user_age_result= String.valueOf(user_age);

        String managerid=getIntent().getStringExtra("managerid");
        String chargeid=getIntent().getStringExtra("listnumber");
        String date=getIntent().getStringExtra("date");
        int situation=getIntent().getIntExtra("situation",-1);

        String gas=getIntent().getStringExtra("gas");
        String water=getIntent().getStringExtra("water");
        String electricity=getIntent().getStringExtra("electricity");

        float money_real=Float.parseFloat(gas)+Float.parseFloat(water)+Float.parseFloat(electricity);

        String money_final= String.valueOf(money_real);


        userwaterfee_id.setText(user_id);
        userwaterfee_username.setText(user_name);
        userwaterfee_age.setText(user_age_result);

        if(situation==0){
            getwaterfee_situaton.setText("未缴费");
        }else{

            getwaterfee_situaton.setText("已缴费");
        }
        getwaterfee_money.setText(money_final);
        getwaterfee_managerid.setText(managerid);
        getwaterfee_date.setText(date);
        getwaterfee_chargeid.setText(chargeid);

        getwaterfee_eletricty.setText(electricity);
        getwaterfee_water.setText(water);
        getwaterfee_gas.setText(gas);


        paywaterfeeback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        paywaterfee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String,Object> parasm=new HashMap<>();
                parasm.put("uid",user_id);
                parasm.put("cid",chargeid);

                OkHttpUtil.post("http://118.89.234.99:8080/xms/updatecharge",parasm,handler);

            }
        });


    }

    private void initView() {

        userwaterfee_id=findViewById(R.id.userwaterfee_id);
        userwaterfee_age=findViewById(R.id.userwaterfee_age);
        userwaterfee_username=findViewById(R.id.userwaterfee_username);

        getwaterfee_gas=findViewById(R.id.getwaterfee_gas);
        getwaterfee_water=findViewById(R.id.getwaterfee_water);
        getwaterfee_eletricty=findViewById(R.id.getwaterfee_eletricity);

        getwaterfee_chargeid=findViewById(R.id.getwaterfee_chargeid);
        getwaterfee_date=findViewById(R.id.getwaterfee_date);

        getwaterfee_managerid=findViewById(R.id.getwaterfee_managerid);
        getwaterfee_money=findViewById(R.id.getwaterfee_money);
        getwaterfee_situaton=findViewById(R.id.getwaterfee_situation);

        paywaterfeeback=findViewById(R.id.paywaterfee_back);

        paywaterfee=findViewById(R.id.paywaterfee);

    }
}
