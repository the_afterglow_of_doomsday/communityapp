package com.shixun.cms.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Adapter.PropertyfeeAdapter;
import com.shixun.cms.Entity.Propertyfee;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.ArrayList;
import java.util.List;

public class PropertyfeeActivity extends AppCompatActivity {

    private  RecyclerView rvPropertyfee;
    private LinearLayout Propertyfee_loading;
    private ImageView back;
    private Button propertyfee_add;
    List<Propertyfee> data;
    String UID;
    User userInfo;
    int feeType;
    String uid_true;
    private PropertyfeeAdapter propertyfeeAdapter;


    private Handler handler=new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what== Config.STATUS_OK){

                Result result= JSONObject.parseObject(msg.obj.toString(), Result.class);
                JSONArray jsonArray=(JSONArray) result.getData();
                List<Propertyfee> propertyfees=jsonArray.toJavaList(Propertyfee.class);
                System.err.println(propertyfees.toString());

                if (propertyfees.size() == 0) {
                    Toast.makeText(PropertyfeeActivity.this, "没有查询到与您有关的账单信息", Toast.LENGTH_LONG).show();
                    Propertyfee_loading.setVisibility(View.GONE);
                    return;
                }

                data.addAll(propertyfees);
                propertyfeeAdapter.notifyDataSetChanged();
                Propertyfee_loading.setVisibility(View.GONE);

                rvPropertyfee.setVisibility(View.VISIBLE);

            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propertyfee);
//        AndroidBarUtils.setTranslucent(this);

        data = new ArrayList<>();
        Intent intent = getIntent();
        feeType = intent.getIntExtra("feeType", Config.STATUS_FAILURE);
        Initview();

        back.setOnClickListener(view -> finish());

        rvPropertyfee.setLayoutManager(new LinearLayoutManager(this));

        // 如果是物业费用
        if (feeType == Config.FEE_TYPE){
            userInfo = (User) intent.getSerializableExtra("info");
            propertyfeeAdapter=new PropertyfeeAdapter(this,data,feeType);
            propertyfee_add.setOnClickListener(view -> {
                Intent intent1 = new Intent(this, AddBillActivity.class);
                intent1.putExtra("userInfo",userInfo);
                startActivity(intent1);
            });
        }
        else{
            propertyfee_add.setVisibility(View.GONE);
            SharedPreferences sharedPreferences = User.loadLoginUserInfo(this);
            UID = sharedPreferences.getString("user_id", "undefined");
            if ("undefined".equals(UID)){
                Toast.makeText(this,"登录状态异常，请重新登录",Toast.LENGTH_SHORT).show();
                return;
            }
            propertyfeeAdapter=new PropertyfeeAdapter(this,data);
        }

        rvPropertyfee.setAdapter(propertyfeeAdapter);

        uid_true = feeType == 0 ? UID : userInfo.getUser_id();

        OkHttpUtil.get(Config.URL_PROPERTYFEEINFO+uid_true,handler);

    }

    private void Initview() {

        propertyfee_add = findViewById(R.id.propertyfee_add);
        rvPropertyfee=findViewById(R.id.rv_property_fee);
        Propertyfee_loading=findViewById(R.id.propertyfee_loading);
        back=findViewById(R.id.propertyfee_back);
    }

    private void initBroadcast() {
        // 建立IntentFilter对象
        IntentFilter intentFilter = new IntentFilter();
        // 想要监听什么广播,在addAction方法在添加
        intentFilter.addAction("com.shixun.cms.REFRESH_DATA_BILL");
        PropertyfeeActivity.RefreshBroadcastReceiver loadNextPageReceiver = new RefreshBroadcastReceiver();
        // 注册广播接收器
        registerReceiver(loadNextPageReceiver, intentFilter);

    }

    private class RefreshBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("广播","r!!!!!!!!!!!!!!!!!!!!!!");
            rvPropertyfee.setVisibility(View.GONE);
            Propertyfee_loading.setVisibility(View.VISIBLE);
            data.clear();
            propertyfeeAdapter.notifyDataSetChanged();
            // 重新加载数据
            OkHttpUtil.get(Config.URL_PROPERTYFEEINFO+uid_true,handler);
        }
    }
}
