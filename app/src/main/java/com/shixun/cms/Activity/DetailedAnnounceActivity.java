package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Entity.Announce;
import com.shixun.cms.Entity.Manager;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

public class DetailedAnnounceActivity extends AppCompatActivity {

    private int userType;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case Config.STATUS_OK:
                    Toast.makeText(DetailedAnnounceActivity.this,"更新成功", Toast.LENGTH_SHORT).show();
                    // 发送广播提示更新
                    Intent intent = new Intent("com.shixun.cms.REFRESH_DATA_AN");
                    sendBroadcast(intent);
                    finish();
                    // TODO
                case Config.STATUS_ERROR:
                    Toast.makeText(DetailedAnnounceActivity.this,"更新失败", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private Handler handlerDelete = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
            switch (result.getErrorCode()){
                case Config.STATUS_OK:
                    Toast.makeText(DetailedAnnounceActivity.this,"更新成功", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent("com.shixun.cms.REFRESH_DATA_AN");
                    sendBroadcast(intent);
                    finish();
                    // TODO
                case Config.STATUS_ERROR:
                    Toast.makeText(DetailedAnnounceActivity.this,"更新失败", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_announce);
//        AndroidBarUtils.setTranslucent(this);



        Intent intent = getIntent(); // 获取用于启动Activity的Intent
        Announce announce = (Announce) intent.getSerializableExtra("info");
        // 判断调用者：用户或管理员
        userType = intent.getIntExtra("userType", Config.USER_TYPE_USER);

        if (userType == Config.USER_TYPE_USER){
            initUserView(announce);
        }
        else if (userType == Config.USER_TYPE_MANAGER){
            initManagerView(announce);
        }

    }

    private void initManagerView(Announce announce) {
        setContentView(R.layout.activity_detailed_announce_manager);
        ImageView imgBack = findViewById(R.id.announce_manager_detail_back);
        TextView announceDetailTitle = findViewById(R.id.announce_manager_detail_title);
        TextView announceDetailID = findViewById(R.id.announce_manager_detail_id);
        TextView announceDetailManager = findViewById(R.id.announce_manager_detail_manager);
        TextView announceDetailTime = findViewById(R.id.announce_manager_detail_time);
        TextView announceDetailContent = findViewById(R.id.announce_manager_detail_content);

        imgBack.setOnClickListener(view -> finish());
        announceDetailTitle.setText(announce != null ? announce.getTitle() : "Title");
        announceDetailID.setText(announce != null ? announce.getAnnounce_id() : "");
        announceDetailManager.setText(announce != null ? announce.getManager_id() : "");
        announceDetailTime.setText(announce != null ? announce.translateTimeStampToString() : "");
        announceDetailContent.setText(announce != null ? announce.getContent() : "");

        Button delete = findViewById(R.id.announce_manager_delete);

        // 删除
        delete.setOnClickListener(view -> {
            String url = Config.URL_DELETE_ANNOUNCE +"aid=" + announce.getAnnounce_id();
            Log.e("URL------------>", url);
            OkHttpUtil.get(url, handlerDelete);
        });
        Button edit = findViewById(R.id.announce_manager_edit);
        edit.setOnClickListener(view -> {
            // 编辑
            setContentView(R.layout.activity_edit_announce_manager);
            ImageView back = findViewById(R.id.edit_announce_back);
            back.setOnClickListener(v -> finish());

            Button submit = findViewById(R.id.edit_announce_submit);

            submit.setOnClickListener(v->submitUpdatedAnnouncement(announce));

            EditText edit_announce_title = findViewById(R.id.edit_announce_title);
            edit_announce_title.setText(announce != null ? announce.getTitle() : null);

            EditText edit_announce_content = findViewById(R.id.edit_announce_content);
            edit_announce_content.setText(announce != null ? announce.getContent() : null);

            // 显示aid
            TextView tvAID =  findViewById(R.id.edit_announce_aid);
            tvAID.setText(announce != null ? announce.getAnnounce_id() : null);
        });
    }

    private void submitUpdatedAnnouncement(Announce announce) {
        SharedPreferences preferences = Manager.loadLoginManagerInfo(this);
        String mid = preferences.getString("manager_id", "admin");
        EditText send_announce_title = findViewById(R.id.edit_announce_title);
        String title = send_announce_title.getText().toString();
        if ("".equals(title)){
            Toast.makeText(this, "标题不可为空", Toast.LENGTH_SHORT).show();
            return;
        }

        EditText send_announce_content = findViewById(R.id.edit_announce_content);
        String content = send_announce_content.getText().toString();

        long currentTimeMillis = System.currentTimeMillis();

        String url = Config.URL_UPDATE_ANNOUNCE + "aid=" + announce.getAnnounce_id()
                +"&mid=" + mid
                +"&tit=" + title
                +"&con=" + content
                +"&sdt=" + currentTimeMillis;
        Log.e("URL------------>", url);
        OkHttpUtil.get(url, handler);
    }

    private void initUserView(Announce announce) {
        ImageView imgBack = findViewById(R.id.announce_detail_back);
        TextView announceDetailTitle = findViewById(R.id.announce_detail_title);
        TextView announceDetailID = findViewById(R.id.announce_detail_id);
        TextView announceDetailManager = findViewById(R.id.announce_detail_manager);
        TextView announceDetailTime = findViewById(R.id.announce_detail_time);
        TextView announceDetailContent = findViewById(R.id.announce_detail_content);

        imgBack.setOnClickListener(view -> finish());
        announceDetailTitle.setText(announce != null ? announce.getTitle() : "Title");
        announceDetailID.setText(announce != null ? announce.getAnnounce_id() : "");
        announceDetailManager.setText(announce != null ? announce.getManager_id() : "");
        announceDetailTime.setText(announce != null ? announce.translateTimeStampToString() : "");
        announceDetailContent.setText(announce != null ? announce.getContent() : "");
    }
}
