package com.shixun.cms.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.shixun.cms.R;

public class SuccesspayfeeActivity extends AppCompatActivity {

    private ImageView successpayfee_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successpayfee);

        successpayfee_back=findViewById(R.id.successpayfee_back);

        successpayfee_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SuccesspayfeeActivity.this,MainActivity.class));
                finish();
            }
        });
    }
}
