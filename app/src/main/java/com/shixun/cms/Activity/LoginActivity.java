package com.shixun.cms.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Entity.Manager;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;
import com.shixun.cms.Util.PreferenceUtil;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText login_username, login_password;
    private Button login, to_register;
    private String uaccount, upassword;

    private CheckBox checkusername,autologin,checkadminisator;
    private long mExitTime;

    Handler handlerManager =new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            //登陆失败
            if (msg.what == Config.STATUS_OK) {//网络请求成功，但响应需要根据实际来操作
                Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                if (result.getErrorCode() == Config.STATUS_SUCCESS) {
                    // 登陆成功
                    // 保存登录信息
                    JSONObject object = (JSONObject) result.getData();
                    Manager managerInfo = object.toJavaObject(Manager.class);
                    Manager.saveLoginManagerInfo(LoginActivity.this, managerInfo);
                    Toast.makeText(LoginActivity.this, "登陆成功", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(LoginActivity.this, ManagerMainActivity.class));
                    finish();
                    System.err.println("登录成功");
                    System.err.println(msg.obj.toString());
                } else {
                    //登陆失败
                    Toast.makeText(LoginActivity.this, "登陆失败", Toast.LENGTH_LONG).show();
                    System.err.println("登录失败");
                    System.err.println(result.getErrorCode());
                    System.err.println(msg.obj.toString());
                }
            } else {
                Toast.makeText(LoginActivity.this, "登陆失败", Toast.LENGTH_LONG).show();
                System.err.println("登录失败");
                System.err.println(msg.what);
                System.err.println(msg.obj.toString());
            }
        }
    };
    Handler handler=new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            //登陆失败
            if (msg.what == Config.STATUS_OK) {//网络请求成功，但响应需要根据实际来操作
                Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                if (result.getErrorCode() == Config.STATUS_SUCCESS) {
                    // 登陆成功
                    // 保存登录信息
//
                    JSONObject object = (JSONObject) result.getData();
                    User userInfo = object.toJavaObject(User.class);
                    User.saveLoginUserInfo(LoginActivity.this, userInfo);
                    Toast.makeText(LoginActivity.this, "登陆成功", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                    System.err.println("登录成功");
                } else {
                    //登陆失败
                    Toast.makeText(LoginActivity.this, "登陆失败", Toast.LENGTH_LONG).show();
                    System.err.println("登录失败");
                    System.err.println(result.getErrorCode());
                    System.err.println(msg.obj.toString());
                }
            } else {
                Toast.makeText(LoginActivity.this, "登陆失败", Toast.LENGTH_LONG).show();
                System.err.println("登录失败");
                System.err.println(msg.what);
                System.err.println(msg.obj.toString());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        AndroidBarUtils.setTranslucent(this);
        InitView();
        InitBind();
        InitEvent();

    }

    private void InitEvent() {
        to_register.setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));

        login.setOnClickListener(view -> {
            uaccount = login_username.getText().toString();
            upassword = login_password.getText().toString();

            if (uaccount.trim().length()==0) {
                login_username.setError("用户名不能为空");
            }

            if (upassword.trim().length()==0) {
                login_password.setError("密码不能为空");
            }

            // 是否记住用户
            if (checkusername.isChecked()) {
                PreferenceUtil.putBoolean(LoginActivity.this, Config.SETTTING_REMEMBER, true);
                PreferenceUtil.putString(LoginActivity.this, Config.DATA_USERNAME, uaccount);
                PreferenceUtil.putString(LoginActivity.this, Config.DATA_PASSWORD, upassword);

                // 是否自动登录
                if (autologin.isChecked()) {
                    PreferenceUtil.putBoolean(LoginActivity.this, Config.SETTTING_AUTO_LOGIN, true);

                    // 是否为管理员
                    if (checkadminisator.isChecked()){
                        PreferenceUtil.putBoolean(LoginActivity.this, Config.SETTTING_AUTO_LOGIN_MANAGER, true);
                    }
                    else PreferenceUtil.putBoolean(LoginActivity.this, Config.SETTTING_AUTO_LOGIN_MANAGER, false);
                } else {
                    PreferenceUtil.putBoolean(LoginActivity.this, Config.SETTTING_AUTO_LOGIN, false);
                }
            } else {
                PreferenceUtil.putBoolean(LoginActivity.this, Config.SETTTING_REMEMBER, false);
                PreferenceUtil.putBoolean(LoginActivity.this, Config.SETTTING_AUTO_LOGIN, false);
            }
            if(checkadminisator.isChecked()){
                loginadministrator();

            }else{
                loginuser();
            }

        });

        checkusername.setOnCheckedChangeListener((compoundButton, b) -> {
            if(!b){
                autologin.setChecked(false);
            }
        });

        autologin.setOnCheckedChangeListener((compoundButton, b) -> checkusername.setChecked(true));
    }

    private void loginuser() {
        Map<String, Object> param = new HashMap<>();
        param.put("uaccount",login_username.getText().toString());
        param.put("upassword",login_password.getText().toString());
        OkHttpUtil.post(Config.URL_LOGIN,param,handler);
    }


    private void loginadministrator(){
        Map<String, Object> param = new HashMap<>();
        param.put("maccount",login_username.getText().toString());
        param.put("mpassword",login_password.getText().toString());
        OkHttpUtil.post(Config.URL_ADMIN,param, handlerManager);

    }

    private void InitView () {
        login_username = findViewById(R.id.logon_username);
        login_password = findViewById(R.id.login_password);
        login = findViewById(R.id.bt_login);
        to_register = findViewById(R.id.bt_toregister);
        checkusername=findViewById(R.id.checkusername);
        autologin=findViewById(R.id.autologin);
        checkadminisator=findViewById(R.id.checkadministrator);
    }


    private void InitBind() {
        if (PreferenceUtil.getBoolean(this, Config.SETTTING_REMEMBER)) {
            checkusername.setChecked(true);

            login_username.setText(PreferenceUtil.getString(this, Config.DATA_USERNAME));
            login_password.setText(PreferenceUtil.getString(this, Config.DATA_PASSWORD));

            // 自动登录
            if (PreferenceUtil.getBoolean(this, Config.SETTTING_AUTO_LOGIN)) {

                // 检查自动登陆的类型
                if (PreferenceUtil.getBoolean(this, Config.SETTTING_AUTO_LOGIN_MANAGER)){
                    autologin.setChecked(true);
                    loginadministrator();
                }
                else{
                    autologin.setChecked(true);
                    loginuser();
                }

            }
        }
    }

    // 连按两次退出
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            Toast.makeText(LoginActivity.this, "再按一次退出", Toast.LENGTH_SHORT).show();
            mExitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }
}


