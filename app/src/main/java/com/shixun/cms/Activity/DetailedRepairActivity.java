package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shixun.cms.Entity.Repair;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.Calendar;

public class DetailedRepairActivity extends AppCompatActivity {
    private String payTimePicked;
    private String repairTimePicked;
    private TextView repairTime;
    private TextView payTime;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == Config.STATUS_OK){
                Toast.makeText(DetailedRepairActivity.this, "修改状态成功", Toast.LENGTH_SHORT).show();
                // 发送广播提示更新
                Intent intent = new Intent("com.shixun.cms.REFRESH_DATA");
                sendBroadcast(intent);
                finish();
            }
            else{
                Toast.makeText(DetailedRepairActivity.this, "修改状态失败", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_repair);
//        AndroidBarUtils.setTranslucent(this);

        Intent intent = getIntent();
        Repair repair = (Repair) intent.getSerializableExtra("info");
        // 获取登陆用户的类型
        int userType = intent.getIntExtra("userType", Config.UNDEFINED);

        // 数据传递有问题
        if (userType == Config.UNDEFINED){
            Toast.makeText(this,"用户状态异常",Toast.LENGTH_SHORT).show();
        }

        // 如果是普通用户
        else if (userType == Config.USER_TYPE_USER){
            if (repair != null) {
                initUserAction(repair);
            }
        }
        else if (userType == Config.USER_TYPE_MANAGER){
            if (repair != null) {
                initManagerAction(repair);
            }
        }
    }

    private void initManagerAction(Repair repair) {
        setContentView(R.layout.activity_detailed_repair_manager);

        TextView id =findViewById(R.id.manager_update_repair_id);
        id.setText(repair.getRepair_id());

        TextView status = findViewById(R.id.manager_update_status);
        String statusString = repair.getRepait_status() == 0? "处理中":"已处理";
        if (repair.getRepait_status() == 0){
            status.setText(statusString);
            status.setTextColor(ContextCompat.getColor(this,R.color.red));
        }
        else if (repair.getRepait_status() == 1){
            status.setText(statusString);
            status.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));
        }

        TextView manager = findViewById(R.id.manager_update_manager);
        manager.setText(repair.getManager_id());

        TextView type = findViewById(R.id.manager_update_type);
        type.setText(repair.getRepair_type());

        TextView reportTime = findViewById(R.id.manager_update_report_time);
        reportTime.setText(repair.translateDateToString(repair.getReport_time()));

        TextView exceptTime = findViewById(R.id.manager_update_hope_time);
        exceptTime.setText(repair.translateDateToString(repair.getHope_time()));

        // 选时间
        repairTime = findViewById(R.id.manager_update_fixed_time);
        repairTime.setText(repair.translateDateToString(repair.getRepair_time()));
        repairTime.setOnClickListener(view -> {
            chooseRepairTime();
        });


        EditText principle = findViewById(R.id.manager_update_principle);
        principle.setText(repair.getPrincipal());

        EditText fee = findViewById(R.id.manager_update_charge_1);
        fee.setText(String.valueOf(repair.getRepair_charge()));


        payTime = findViewById(R.id.manager_update_pay_data);
        payTime.setText(repair.translateDateToString(repair.getPay_date()));
        payTime.setOnClickListener(view -> {
            choosePayTimePicked();
        });


        TextView detail = findViewById(R.id.manager_update_content);
        detail.setText(repair.getDetail());

        ImageView back = findViewById(R.id.manager_update_repair_back);
        back.setOnClickListener(view -> finish());

        Button submit = findViewById(R.id.manager_update_submit);
        submit.setOnClickListener(view -> {
            String principleSetted = principle.getText().toString();
            float feeSetted = Float.parseFloat(fee.getText().toString());
            String url = Config.URL_UPDATE_REPAIR + "uid="+repair.getUser_id()+
                    "&rid="+repair.getRepair_id() +
                    "&rprt=" + repairTimePicked +
                    "&pri=" + principleSetted +
                    "&rpc=" + (int)feeSetted +
                    "&pdt=" + payTimePicked;
            Log.e("URL------->", url);
            OkHttpUtil.get(url, handler);
        });
    }

    private void chooseRepairTime() {
        final String[] hopeTime = new String[1];
        Calendar calendar = Calendar.getInstance();
        //create a datePickerDialog and then shoe it on your screen
        new DatePickerDialog(DetailedRepairActivity.this,//binding the listener for your DatePickerDialog
                (view, year, month, dayOfMonth) -> {
                    month++;
                    Log.e("DatePicker","Year:" + year + " Month:" + month + " Day:" + dayOfMonth);
                    if (month>0 && month<10 && dayOfMonth>0&&dayOfMonth<10){
                        hopeTime[0] = year +"-0"+ month +"-0"+dayOfMonth;
                    }
                    else if (month>0 && month<10){
                        hopeTime[0] = year +"-0"+ month +"-"+dayOfMonth;
                    }
                    else if (dayOfMonth>0&&dayOfMonth<10){
                        hopeTime[0] = year +"-"+ month +"-0"+dayOfMonth;
                    }
                    else  hopeTime[0] = year +"-"+ month +"-"+dayOfMonth;
                    repairTimePicked = hopeTime[0];
                    repairTime.setText(repairTimePicked);
                }
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void initUserAction(Repair repair) {
        TextView id =findViewById(R.id.detail_repair_id);
        id.setText(repair.getRepair_id());

        TextView status = findViewById(R.id.detail_repair_status);
        String statusString = repair.getRepait_status() == 0? "处理中":"已处理";
        if (repair.getRepait_status() == 0){
            status.setText(statusString);
            status.setTextColor(ContextCompat.getColor(this,R.color.red));
        }
        else if (repair.getRepait_status() == 1){
            status.setText(statusString);
            status.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));
        }

        TextView manager = findViewById(R.id.detail_repair_manager);
        manager.setText(repair.getManager_id());

        TextView type = findViewById(R.id.detail_repair_type);
        type.setText(repair.getRepair_type());

        TextView reportTime = findViewById(R.id.detail_repair_report_time);
        reportTime.setText(repair.translateDateToString(repair.getReport_time()));

        TextView exceptTime = findViewById(R.id.detail_repair_hope_time);
        exceptTime.setText(repair.translateDateToString(repair.getHope_time()));

        TextView repairTime = findViewById(R.id.detail_repair_fixed_time);
        repairTime.setText(repair.translateDateToString(repair.getRepair_time()));

        TextView principle = findViewById(R.id.detail_repair_principle);
        principle.setText(repair.getPrincipal());

        TextView fee = findViewById(R.id.detail_repair_charge_1);
        fee.setText(String.valueOf(repair.getRepair_charge()));

        TextView payTime = findViewById(R.id.detail_repair_pay_data);
        payTime.setText(repair.translateDateToString(repair.getPay_date()));

        TextView detail = findViewById(R.id.detail_repair_content);
        detail.setText(repair.getDetail());

        ImageView back = findViewById(R.id.detail_repair_back);
        back.setOnClickListener(view -> finish());
    }

    // 时间选择器
    private void choosePayTimePicked() {

        Calendar calendar = Calendar.getInstance();
        //create a datePickerDialog and then shoe it on your screen
        new DatePickerDialog(DetailedRepairActivity.this,//binding the listener for your DatePickerDialog
                (view, year, month, dayOfMonth) -> {
                    month++;
                    String hopeTime;
                    Log.e("DatePicker","Year:" + year + " Month:" + month + " Day:" + dayOfMonth);
                    if (month>0 && month<10 && dayOfMonth>0&&dayOfMonth<10){
                        hopeTime = year +"-0"+ month +"-0"+dayOfMonth;
                    }
                    else if (month>0 && month<10){
                        hopeTime = year +"-0"+ month +"-"+dayOfMonth;
                    }
                    else if (dayOfMonth>0&&dayOfMonth<10){
                        hopeTime= year +"-"+ month +"-0"+dayOfMonth;
                    }
                    else  hopeTime = year +"-"+ month +"-"+dayOfMonth;
                    payTimePicked = hopeTime;
                    payTime.setText(payTimePicked);
                }
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH)).show();

    }
}