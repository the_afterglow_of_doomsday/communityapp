package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Entity.House;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

public class EditHouseInfoActivity extends AppCompatActivity {
    private ImageView back;
    private TextView houseinfo_edit_hid, houseinfo_edit_uid;
    private EditText houseinfo_edit_floor, houseinfo_edit_area, houseinfo_edit_address;
    private Button houseinfo_edit_update;
    private Boolean isUpdate = true;
    String newHID, newUID;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            String tipsSuccess = isUpdate?"更新成功":"新增成功";
            String tipsFailed =  isUpdate?"更新失败":"新增失败";
            Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
            if (result.getErrorCode() == Config.STATUS_OK){
                Toast.makeText(EditHouseInfoActivity.this, tipsSuccess,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent("com.shixun.cms.REFRESH_DATA_HOUSE");
                sendBroadcast(intent);
                finish();
            }else {
                Log.e("EXCEPTION----------->", result.getMsg());
                Toast.makeText(EditHouseInfoActivity.this, tipsFailed,Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_house_info);
        Intent intent = getIntent();
        // 如果是新增
        if (intent.getBooleanExtra("insert",false)) {
            isUpdate = false;
            newHID = intent.getStringExtra("insertHID");
            newUID = intent.getStringExtra("insertUID");
        }
            House house = (House) intent.getSerializableExtra("info");

            back = findViewById(R.id.houseinfo_edit_back);
            back.setOnClickListener(view -> finish());
            houseinfo_edit_hid = findViewById(R.id.houseinfo_edit_hid);
            houseinfo_edit_uid = findViewById(R.id.houseinfo_edit_uid);
            houseinfo_edit_floor = findViewById(R.id.houseinfo_edit_floor);
            houseinfo_edit_area = findViewById(R.id.houseinfo_edit_area);
            houseinfo_edit_address = findViewById(R.id.houseinfo_edit_address);
            houseinfo_edit_update = findViewById(R.id.houseinfo_edit_update);

            if (!isUpdate){
                houseinfo_edit_hid.setText(newHID);
                houseinfo_edit_uid.setText(newUID);
            }
            else{
                houseinfo_edit_hid.setText(house != null ? house.getHouse_id() : null);
                houseinfo_edit_uid.setText(house != null ? house.getUser_id() : null);
            }

            String floor1 = String.valueOf(house != null ? house.getFloor() : 0);
            houseinfo_edit_floor.setText(floor1);
            String area1 = String.valueOf(house != null ? house.getArea() : 0);
            houseinfo_edit_area.setText(area1);
            houseinfo_edit_address.setText(house != null ? house.getAddress() : null);

            houseinfo_edit_update.setOnClickListener(view -> {
                String floor = houseinfo_edit_floor.getText().toString();
                String area = houseinfo_edit_area.getText().toString();
                String address = houseinfo_edit_address.getText().toString();

                if (isUpdate){
                    String url = Config.URL_UPDATE_HOUSEINFO +
                            "hid=" + (house != null ? house.getHouse_id() : "")
                            +"&uid=" + (house != null ? house.getUser_id() : "")
                            +"&flr=" + floor
                            +"&area=" + area
                            +"&add="+address;
                    OkHttpUtil.get(url,handler);
                }
                else{
                    String url = Config.URL_INSERT_HOUSEINFO +
                            "hid=" + newHID
                            +"&uid=" + newUID
                            +"&flr=" + floor
                            +"&area=" + area
                            +"&add="+address;
                    Log.e("URL------------>", url);

                    OkHttpUtil.get(url,handler);
                }
            });
        }

    }
