package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Adapter.RepairFormAdapter;
import com.shixun.cms.Entity.Manager;
import com.shixun.cms.Entity.Repair;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.ArrayList;
import java.util.List;

public class RepairListActivity extends AppCompatActivity {
    RecyclerView rvRepairInfo;
    private LinearLayout repairFormLoading;
    private RepairFormAdapter repairFormAdapter;
    private ImageView back;
    private List<Repair> data;
    private String UID;
    private int userType;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == Config.STATUS_OK) {
                Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                JSONArray jsonArray = (JSONArray) result.getData();
                List<Repair> repairList = jsonArray.toJavaList(Repair.class);

                // <Activity复用 处理管理员逻辑>
                // 只加载与自己相关的数据
                if (userType == Config.USER_TYPE_MANAGER){
                    SharedPreferences managerInfo = Manager.loadLoginManagerInfo(RepairListActivity.this);
                    String mid = managerInfo.getString("manager_id", "admin");
                    List<Repair> managerRelatedRepair = new ArrayList<>();
                    for (Repair repair : repairList) {
                        if (repair.getManager_id().equals(mid)){
                            managerRelatedRepair.add(repair);
                        }
                    }
                    repairList.clear();
                    repairList = managerRelatedRepair;
                }
                //</Activity复用 处理管理员逻辑>

                if (repairList.size() == 0) {
                    Toast.makeText(RepairListActivity.this, "没有查询到与您有关的维修工单", Toast.LENGTH_LONG).show();
                    repairFormLoading.setVisibility(View.GONE);
                    return;
                }
                data.addAll(repairList);
                repairFormAdapter.notifyDataSetChanged();
                repairFormLoading.setVisibility(View.GONE);
                rvRepairInfo.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repair_list);
//        AndroidBarUtils.setTranslucent(this);
        data = new ArrayList<>();


        // 注册消息广播
        initBroadcast();
        // 判断调用者：用户或管理员
        Intent intent = getIntent();
        userType = intent.getIntExtra("userType", Config.USER_TYPE_USER);


        if (userType == Config.USER_TYPE_USER){
            // 尝试获得普通用户信息
            SharedPreferences sharedPreferences = User.loadLoginUserInfo(this);
            UID = sharedPreferences.getString("user_id", "undefined");
            if ("undefined".equals(UID)){
                Toast.makeText(this,"登录状态异常，请重新登录",Toast.LENGTH_SHORT).show();
                return;
            }
        }

        repairFormLoading = findViewById(R.id.repaireform_loading);
        back = findViewById(R.id.repairform_back);
        rvRepairInfo = findViewById(R.id.rv_repairform_info);
        rvRepairInfo.setLayoutManager(new LinearLayoutManager(this));
        data = new ArrayList<>();
        repairFormAdapter = new RepairFormAdapter(this,data, userType);
        rvRepairInfo.setAdapter(repairFormAdapter);
        back = findViewById(R.id.repairform_back);
        back.setOnClickListener(v->finish());
        initData();
    }

    private void initBroadcast() {
        // 建立IntentFilter对象
        IntentFilter intentFilter = new IntentFilter();
        // 想要监听什么广播,在addAction方法在添加
        intentFilter.addAction("com.shixun.cms.REFRESH_DATA");
        RefreshBroadcastReceiver loadNextPageReceiver = new RefreshBroadcastReceiver();
        // 注册广播接收器
        registerReceiver(loadNextPageReceiver, intentFilter);

    }

    private void initData() {
        if (userType == Config.USER_TYPE_USER){
            OkHttpUtil.get(Config.URL_REPAIR_ME + UID, handler);
        }
        else if (userType == Config.USER_TYPE_MANAGER){
            // dm1oKnHcL!jh*7sW为约定的一个字符串，指示后端返回所有信息，无实际含义
            // TODO
            OkHttpUtil.get(Config.URL_REPAIR_ME + "dm1oKnHcL!jh*7sW", handler);
        }
    }


    private class RefreshBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            rvRepairInfo.setVisibility(View.GONE);
            repairFormLoading.setVisibility(View.VISIBLE);
            data.clear();
            repairFormAdapter.notifyDataSetChanged();
            // 重新加载数据
            initData();
        }
    }
}
