package com.shixun.cms.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private EditText community_city,community_name;

    private EditText user_username,user_password,user_againpassword,user_age,user_phone,user_id,user_surepassword;

    private Button register;

    private String communitycity,communityname,userusername,userage,userpassword,useragainpassword,userphone,userid,usersurepassword;

    private   int checkrepeat;

    private Handler handler1=new Handler(Looper.myLooper()){


        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {

                case Config.STATUS_OK://网络请求成功，但响应需要根据实际来操作
                    Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);

                    if (result.getErrorCode() == Config.STATUS_SUCCESS) {
                        // 登陆成功
                        // 保存信息
                        System.err.println(result.getErrorCode());
                        System.err.println(msg.obj.toString());

                        checkrepeat=1;

                        if(checkrepeat==0){

                            Map<String,Object> param=new HashMap<>();
                            param.put("uname",user_username.getText().toString());
                            param.put("age",user_age.getText().toString());
                            param.put("upassword",user_password.getText().toString());
                            param.put("uaccount",user_againpassword.getText().toString());
                            param.put("utel",user_phone.getText().toString());
                            param.put("uid",user_id.getText().toString());


                            OkHttpUtil.post("http://118.89.234.99:8080/xms/register",param,handler);


                        }else {

                            Toast.makeText(RegisterActivity.this,"用户名已存在,请修改后重试",Toast.LENGTH_LONG).show();

                        }



                    } else {
                        System.err.println(result.getErrorCode());

                        checkrepeat=0;

                        if(checkrepeat==0){

                            Map<String,Object> param=new HashMap<>();
                            param.put("uname",user_username.getText().toString());
                            param.put("age",user_age.getText().toString());
                            param.put("upassword",user_password.getText().toString());
                            param.put("uaccount",user_againpassword.getText().toString());
                            param.put("utel",user_phone.getText().toString());
                            param.put("uid",user_id.getText().toString());


                            OkHttpUtil.post("http://118.89.234.99:8080/xms/register",param,handler);


                        }else {

                            Toast.makeText(RegisterActivity.this,"用户名已存在,请修改后重试",Toast.LENGTH_LONG).show();

                        }




                    }
                    break;
                default://登陆失败
                    Toast.makeText(RegisterActivity.this,"网络连接失败",Toast.LENGTH_LONG).show();
                    System.err.println("注册失败");
                    System.err.println(msg.what);
                    System.err.println(msg.obj.toString());
                    break;
            }

        }
    };


    private Handler handler=new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {

            super.handleMessage(msg);

            switch (msg.what) {

                case Config.STATUS_OK://网络请求成功，但响应需要根据实际来操作
                    Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);

                    if (result.getErrorCode() == Config.STATUS_SUCCESS) {
                        // 登陆成功
                        // 保存信息

                        Toast.makeText(RegisterActivity.this,"注册成功",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        System.err.println("注册成功");
                        finish();

                    } else {
                        //登陆失败
                        Toast.makeText(RegisterActivity.this,"用户id可能已经存在，请修改id后重试",Toast.LENGTH_LONG).show();
                        System.err.println("注册失败");
                        System.err.println(msg.what);
                        System.err.println(msg);
                    }
                    break;
                default://登陆失败
                    Toast.makeText(RegisterActivity.this,"注册失败",Toast.LENGTH_LONG);
                    System.err.println("注册失败");
                    System.err.println(msg.what);
                    System.err.println(msg.obj.toString());
                    break;
            }


        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        AndroidBarUtils.setTranslucent(this);
        InitView();
        InitEvent();


    }

    private void InitEvent() {

        register.setOnClickListener(view -> {

            userusername=user_username.getText().toString();
            userage=user_age.getText().toString();
            userpassword=user_password.getText().toString();
            useragainpassword=user_againpassword.getText().toString();
            userphone=user_phone.getText().toString();
            userid=user_id.getText().toString();
            usersurepassword=user_surepassword.getText().toString();

            if(userusername.trim().length()==0||userage.trim().length()==0||userpassword.trim().length()==0||useragainpassword.trim().length()==0||userphone.trim().length()==0||userid.trim().length()==0){

                Toast.makeText(RegisterActivity.this,"用户信息不完整",Toast.LENGTH_LONG).show();
            }
            if(userpassword.equals(usersurepassword)){

                registeruser();

            }
            else {


               Toast.makeText(RegisterActivity.this,"请确认两次密码输入相同",Toast.LENGTH_LONG).show();

            }


        });

    }

    private void registeruser() {


        Map<String,Object> parasm2=new HashMap<>();

        parasm2.put("uaccount",user_againpassword.getText().toString());

        OkHttpUtil.post(Config.URL_CHECKUSERACCOUNT,parasm2,handler1);


    }

    private void InitView() {


        user_username=findViewById(R.id.user_username);
        user_age=findViewById(R.id.user_age);
        user_againpassword=findViewById(R.id.user_againpassword);
        user_password=findViewById(R.id.user_password);
        user_phone=findViewById(R.id.user_phone);
        user_id=findViewById(R.id.user_id);
        user_surepassword=findViewById(R.id.user_surepassword);


        register=findViewById(R.id.register);

    }
}
