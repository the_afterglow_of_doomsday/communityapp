package com.shixun.cms.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Adapter.HouseInfoAdapter;
import com.shixun.cms.Entity.House;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.ArrayList;
import java.util.List;

public class HouseInfoActivity extends AppCompatActivity {
    private int userType;
    private User user;
    RecyclerView rvHouseInfo, rvHouseInfoManager;
    private LinearLayout houseinfo_loading, houseinfo_loadingManager;
    private HouseInfoAdapter houseInfoAdapter;
    private List<House> data, dataManager;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
            if (result.getErrorCode() == Config.STATUS_OK) {
                JSONArray jsonArray = (JSONArray) result.getData();
                List<House> houses = jsonArray.toJavaList(House.class);
                if (houses.size() == 0) {
                    Toast.makeText(HouseInfoActivity.this, "没有查询到与您有关的房屋信息，请联系管理员", Toast.LENGTH_SHORT).show();

                    houseinfo_loading.setVisibility(View.GONE);
                    return;
                }
                data.addAll(houses);
                houseInfoAdapter.notifyDataSetChanged();
                houseinfo_loading.setVisibility(View.GONE);
                rvHouseInfo.setVisibility(View.VISIBLE);
            }
        }
    };

    private Handler handlerManager = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
            if (result.getErrorCode() == Config.STATUS_OK) {
                JSONArray jsonArray = (JSONArray) result.getData();
                List<House> houses = jsonArray.toJavaList(House.class);
                if (houses.size() == 0) {
                    Toast.makeText(HouseInfoActivity.this, "没有查询到有关的房屋信息，可以进行添加操作", Toast.LENGTH_SHORT).show();
                    houseinfo_loadingManager.setVisibility(View.GONE);
                    return;
                }
                dataManager.addAll(houses);
                houseInfoAdapter.notifyDataSetChanged();
                houseinfo_loadingManager.setVisibility(View.GONE);
                rvHouseInfoManager.setVisibility(View.VISIBLE);
            }
            else{
                Toast.makeText(HouseInfoActivity.this, "Error Occurred", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_info);
//        AndroidBarUtils.setTranslucent(this);
        initBroadcast();
        // 初始化数据
        data = new ArrayList<>();

        //尝试获取用户类型
        Intent intent = getIntent();
        userType = intent.getIntExtra("userType", Config.USER_TYPE_USER);
        if (userType == Config.USER_TYPE_MANAGER){

            setContentView(R.layout.activity_detailed_house_info);
            dataManager = new ArrayList<>();
            // 以下是管理员的功能
            Button houseinfo_update_add = findViewById(R.id.houseinfo_update_add);
            houseinfo_update_add.setOnClickListener(view -> {
                // 打开新增
                Intent intent1 = new Intent(this, EditHouseInfoActivity.class);
                intent1.putExtra("insert", true);
                intent1.putExtra("insertUID", user.getUser_id());
                intent1.putExtra("insertHID", "H"+System.currentTimeMillis()/100);
                startActivity(intent1);
            });
            user = (User)intent.getSerializableExtra("info");
            houseinfo_loadingManager = findViewById(R.id.houseinfo_update_loading);
            rvHouseInfoManager = findViewById(R.id.rv_update_house_info);
            ImageView back = findViewById(R.id.houseinfo_update_back);
            rvHouseInfoManager.setLayoutManager(new LinearLayoutManager(this));
            back.setOnClickListener(view -> finish());
            // 绑定recycleview 与 adapter
            houseInfoAdapter = new HouseInfoAdapter(this, dataManager, Config.USER_TYPE_MANAGER);
            rvHouseInfoManager.setAdapter(houseInfoAdapter);
            Log.e("URL============>", Config.URL_HOUSEINFO + user.getUser_id());
            OkHttpUtil.get(Config.URL_HOUSEINFO + user.getUser_id(), handlerManager);
        }
        else{
            //以下是普通用户的功能
            SharedPreferences sharedPreferences = User.loadLoginUserInfo(this);
            String UID = sharedPreferences.getString("user_id", "undefined");
            if ("undefined".equals(UID)){
                Toast.makeText(this,"登录状态异常，请重新登录",Toast.LENGTH_SHORT).show();
                return;
            }
            houseinfo_loading = findViewById(R.id.houseinfo_loading);
            rvHouseInfo = findViewById(R.id.rv_house_info);
            ImageView back = findViewById(R.id.houseinfo_back);


            back.setOnClickListener(view -> finish());

            rvHouseInfo.setLayoutManager(new LinearLayoutManager(this));
            // 绑定recycleview 与 adapter
            houseInfoAdapter = new HouseInfoAdapter(this, data, Config.USER_TYPE_USER);
            rvHouseInfo.setAdapter(houseInfoAdapter);
            OkHttpUtil.get(Config.URL_HOUSEINFO + UID, handler);
        }
    }

    private void initBroadcast() {
        // 建立IntentFilter对象
        IntentFilter intentFilter = new IntentFilter();
        // 想要监听什么广播,在addAction方法在添加
        intentFilter.addAction("com.shixun.cms.REFRESH_DATA_HOUSE");
        HouseInfoActivity.RefreshBroadcastReceiver loadNextPageReceiver = new RefreshBroadcastReceiver();
        // 注册广播接收器
        registerReceiver(loadNextPageReceiver, intentFilter);
    }

    private class RefreshBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("广播","r!!!!!!!!!!!!!!!!!!!!!!");
            rvHouseInfoManager.setVisibility(View.GONE);
            houseinfo_loadingManager.setVisibility(View.VISIBLE);
            dataManager.clear();
            houseInfoAdapter.notifyDataSetChanged();
            // 重新加载数据
            initManagerData();
        }
    }

    private void initManagerData() {
        OkHttpUtil.get(Config.URL_HOUSEINFO + user.getUser_id(), handlerManager);
    }
}
