package com.shixun.cms.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.QQUtil;

public class FeedbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
//        AndroidBarUtils.setTranslucent(this);

        Button submit = findViewById(R.id.feedback_submit);
        submit.setOnClickListener(v-> Toast.makeText(FeedbackActivity.this,"反馈成功！", Toast.LENGTH_SHORT).show());

        findViewById(R.id.feedback_back).setOnClickListener(v->finish());
        findViewById(R.id.feedback_qqgroup).setOnClickListener(v->new QQUtil(this).joinQQGroup(Config.REQUEST_QQGROUP_KEY));
    }
}