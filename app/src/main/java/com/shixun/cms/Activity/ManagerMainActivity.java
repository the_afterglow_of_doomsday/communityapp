package com.shixun.cms.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.shixun.cms.Adapter.UserListAdapter;
import com.shixun.cms.Entity.Manager;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.PreferenceUtil;

public class ManagerMainActivity extends AppCompatActivity {

    private LinearLayout manager_toannounce,manager_tomyinformation,manager_send_announce,manager_torepair, manager_house_manage;

    private LinearLayout manager_tosuggestion,manager_tomycommunity,manager_tosetting,manager_torepairlist, manager_wuye_fee;

    private Button manager_bt_logout;

    public ManagerMainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_main);
//        AndroidBarUtils.setTranslucent(this);

        initView();
        initEvent();


    }

    private void initEvent() {

        // 发布公告
        manager_bt_logout.setOnClickListener(v->{
            startActivity(new Intent(ManagerMainActivity.this,FeedbackActivity.class));
        });



        manager_tosetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ManagerMainActivity.this,SettingActivity.class));

            }
        });

        // 处理报修
        manager_torepairlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManagerMainActivity.this,RepairListActivity.class);
                // 传递为管理员登录
                intent.putExtra("userType", Config.USER_TYPE_MANAGER);
                startActivity(intent);
            }
        });


        // 管理员信息
        manager_tomyinformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ManagerMainActivity.this, ManagerInfoActivity.class));

            }
        });

        manager_tomycommunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ManagerMainActivity.this,SupportActivity.class));

            }
        });

        // 查看公告
        manager_toannounce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ManagerMainActivity.this,AnnounceActivity.class);
                // 传递为管理员登录
                intent.putExtra("userType", Config.USER_TYPE_MANAGER);
                startActivity(intent);

            }
        });


        // 退出
        manager_bt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PreferenceUtil.putBoolean(ManagerMainActivity.this, Config.SETTTING_REMEMBER, false);
                PreferenceUtil.putBoolean(ManagerMainActivity.this, Config.SETTTING_AUTO_LOGIN, false);

                Manager.deleteLoginInfo(ManagerMainActivity.this);
                Intent intent = new Intent(ManagerMainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        manager_send_announce.setOnClickListener(view -> {
            startActivity(new Intent(ManagerMainActivity.this, ManagerSendNewAnnounceActivity.class));

        });

        manager_house_manage.setOnClickListener(view -> {
            startActivity(new Intent(ManagerMainActivity.this, UserListActivity.class));
        });

        // 物业费管理
        manager_wuye_fee.setOnClickListener(view -> {
            Intent intent = new Intent(ManagerMainActivity.this, UserListActivity.class);
            intent.putExtra("feeType",Config.FEE_TYPE);
            startActivity(intent);
        });
    }

    private void initView() {
        manager_house_manage = findViewById(R.id.manager_house_manage);

        manager_toannounce=findViewById(R.id.manager_toannounce);
        manager_tomycommunity=findViewById(R.id.manager_tomycommunity);

        manager_tomyinformation=findViewById(R.id.manager_tomyinformation);
        manager_torepairlist=findViewById(R.id.manager_torepair);

        manager_tosetting=findViewById(R.id.manager_tosettings);

        manager_bt_logout=findViewById(R.id.manager_bt_logout);

        manager_send_announce = findViewById(R.id.manager_send_announce);

        manager_wuye_fee = findViewById(R.id.manager_wuye_fee);

    }
}
