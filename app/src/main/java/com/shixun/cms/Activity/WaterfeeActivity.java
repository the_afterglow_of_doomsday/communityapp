package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Adapter.PropertyfeeAdapter;
import com.shixun.cms.Adapter.WaterfeeAdapter;
import com.shixun.cms.Entity.Propertyfee;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.ArrayList;
import java.util.List;

public class WaterfeeActivity extends AppCompatActivity {

    private RecyclerView rvwaterfee;
    private LinearLayout waterfee_loading;
    private ImageView back;
    List<Propertyfee> data;
    private WaterfeeAdapter waterfeeAdapter;


    private Handler handler=new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what== Config.STATUS_OK){

                Result result= JSONObject.parseObject(msg.obj.toString(), Result.class);
                JSONArray jsonArray=(JSONArray) result.getData();
                List<Propertyfee> propertyfees=jsonArray.toJavaList(Propertyfee.class);
                System.err.println(propertyfees.toString());

                if (propertyfees.size() == 0) {
                    Toast.makeText(WaterfeeActivity.this, "没有查询到与您有关的账单信息", Toast.LENGTH_LONG).show();
                    waterfee_loading.setVisibility(View.GONE);
                    return;
                }

                data.addAll(propertyfees);
                waterfeeAdapter.notifyDataSetChanged();
                waterfee_loading.setVisibility(View.GONE);

                rvwaterfee.setVisibility(View.VISIBLE);

            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waterfee);
//        AndroidBarUtils.setTranslucent(this);

        data = new ArrayList<>();
        SharedPreferences sharedPreferences = User.loadLoginUserInfo(this);
        String UID = sharedPreferences.getString("user_id", "undefined");
        if ("undefined".equals(UID)){
            Toast.makeText(this,"登录状态异常，请重新登录",Toast.LENGTH_SHORT).show();
            return;
        }

        Initview();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rvwaterfee.setLayoutManager(new LinearLayoutManager(this));

        waterfeeAdapter=new WaterfeeAdapter(this,data);
        rvwaterfee.setAdapter(waterfeeAdapter);

        OkHttpUtil.get(Config.URL_PROPERTYFEEINFO+UID,handler);


    }

    private void Initview() {

        rvwaterfee=findViewById(R.id.rv_water_fee);
        waterfee_loading=findViewById(R.id.waterfee_loading);
        back=findViewById(R.id.waterfee_back);
    }
}

