package com.shixun.cms.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
//        AndroidBarUtils.setTranslucent(this);
        LinearLayout support = findViewById(R.id.setting_support);
        support.setOnClickListener(v->startActivity(new Intent(SettingActivity.this, SupportActivity.class)));

        ImageView back = findViewById(R.id.setting_back);
        back.setOnClickListener(v->finish());
    }
}
