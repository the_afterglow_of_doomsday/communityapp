package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

public class UserInfoActivity extends AppCompatActivity {
    private String currentName;
    private String currentTel;
    private int currentAge;
    private SharedPreferences sharedPreferences;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == Config.STATUS_OK){
                setContentView(R.layout.activity_user_info);
                initShowInfoView();
                updateSharedPreferences();
                Toast.makeText(UserInfoActivity.this,"修改成功",Toast.LENGTH_SHORT).show();
            }
        }
    };

    // 更新本地SharedPreferences
    private void updateSharedPreferences() {
        String id = sharedPreferences.getString("user_id", "undefined");
        String pwd = sharedPreferences.getString("user_pwd", "undefined");
        String account = sharedPreferences.getString("user_account", "undefined");
        User.saveLoginUserInfo(this, new User(id, account, pwd, currentName, currentTel, currentAge));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
//        AndroidBarUtils.setTranslucent(this);

        sharedPreferences = User.loadLoginUserInfo(this);
        currentName = sharedPreferences.getString("user_name","undefined");
        currentTel = sharedPreferences.getString("user_tel","undefined");
        currentAge = sharedPreferences.getInt("age", Integer.MAX_VALUE);
        initShowInfoView();

        ImageView back = findViewById(R.id.userinfo_back);
        back.setOnClickListener(v->finish());

        Button update = findViewById(R.id.userinfo_update_1);
        update.setOnClickListener(v->{
            Log.e("ts","here");
            changeToUpdateUserInfoPage();
        });

    }

    private void initShowInfoView() {
        TextView uid, uname, uphone, uage;
        uid = findViewById(R.id.userinfo_uid);

        String currentID = sharedPreferences.getString("user_id", "undefined");
        uid.setText(currentID);
        uname = findViewById(R.id.userinfo_uname);
        uname.setText(currentName);
        uphone = findViewById(R.id.userinfo_phone);

        uphone.setText(currentTel);
        uage = findViewById(R.id.userinfo_age);

        if (currentAge == Integer.MAX_VALUE){
            uage.setText("undefined");
        }else{
            uage.setText(String.valueOf(currentAge));
        }
    }

    /**
     * 更改为修改用户信息布局
     */
    private void changeToUpdateUserInfoPage() {
        setContentView(R.layout.activity_update_user_info);
        EditText newName,  newTel, newAge;
        newName = findViewById(R.id.userinfo_uname_update);
        newName.setText(currentName);

        newTel = findViewById(R.id.userinfo_phone_update);
        newTel.setText(currentTel);

        newAge = findViewById(R.id.userinfo_age_update);
        if (currentAge == Integer.MAX_VALUE){
            newAge.setText("undefined");
        }else{
            newAge.setText(String.valueOf(currentAge));
        }

        Button submit = findViewById(R.id.userinfo_update);
        submit.setOnClickListener(v->{
            currentName = newName.getText().toString().trim();
            currentAge = Integer.parseInt(newAge.getText().toString().trim());
            currentTel = newTel.getText().toString().trim();

            // TODO 待测试
            String url = Config.URL_UPDATE_USERINFO +
                    "uid=" + sharedPreferences.getString("user_id", "undefined") +
                    "&uname=" + currentName +
                    "&utel=" + currentTel +
                    "&age="+ currentAge;
            Log.e("changeToUpda ", "changeToUpdateUserInfoPage: "+ url);
            OkHttpUtil.get(url, handler);
        });
    }
}