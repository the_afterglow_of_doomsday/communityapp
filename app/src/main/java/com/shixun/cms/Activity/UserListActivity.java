package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Adapter.UserListAdapter;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.ArrayList;
import java.util.List;

public class UserListActivity extends AppCompatActivity {
    private RecyclerView rvUser;
    private LinearLayout loading;
    private ImageView ivBack;
    private List<User> data;
    private UserListAdapter userListAdapter;
    int feeType;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == Config.STATUS_OK) {
                Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                JSONArray jsonArray = (JSONArray) result.getData();
                List<User> userList = jsonArray.toJavaList(User.class);
                data.addAll(userList);
                if (data.size() == 0){
                    Toast.makeText(UserListActivity.this, "数据请求失败，请重试", Toast.LENGTH_LONG).show();
                    loading.setVisibility(View.GONE);
                    return;
                }
                userListAdapter.notifyDataSetChanged();
                loading.setVisibility(View.GONE);
                rvUser.setVisibility(View.VISIBLE);
            }
            else{
                Toast.makeText(UserListActivity.this, "数据请求失败，请刷新重试", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_house_info_manage);

        Intent intent = getIntent();
        feeType = intent.getIntExtra("feeType", Config.STATUS_FAILURE);
//        AndroidBarUtils.setTranslucent(this);
        rvUser = findViewById(R.id.rv_manager_house_back);
        loading = findViewById(R.id.manager_house_back_loading);
        ivBack = findViewById(R.id.manager_house_back);
        ivBack.setOnClickListener(v->finish());

        data = new ArrayList<>();

        if (feeType == Config.FEE_TYPE){
            userListAdapter = new UserListAdapter(this, data, feeType);
        }
        else{
            userListAdapter = new UserListAdapter(this, data);
        }

        rvUser.setLayoutManager(new LinearLayoutManager(this));
        rvUser.setAdapter(userListAdapter);
        // 加载数据
        initData();
    }

    private void initData() {
        OkHttpUtil.get(Config.URL_QUERY_ALL_USER, handler);
    }
}
