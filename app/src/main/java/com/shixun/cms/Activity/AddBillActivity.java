package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Entity.House;
import com.shixun.cms.Entity.Manager;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddBillActivity extends AppCompatActivity {
     ImageView add_bill_back;
     LinearLayout add_bill_loading, add_bill_content;
     TextView add_bill_uid, add_bill_username,
            add_bill_age, add_bill_phone, add_bill_houseid, add_bill_managerid,add_bill_chargeid, add_bill_payday;
     EditText add_bill_water, add_bill_ele, add_bill_gas, add_bill_wuye, add_bill_month ;
     Button add_bill_submit;
     User userInfo;
     SharedPreferences managerInfo;
     ArrayList<House> houseArrayList;
     private AlertDialog alertDialog;

     private Handler handler = new Handler(Looper.myLooper()){
         @Override
         public void handleMessage(@NonNull Message msg) {
             Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
             if (result.getErrorCode() == Config.STATUS_OK){
                 Toast.makeText(AddBillActivity.this, "提交成功", Toast.LENGTH_SHORT).show();
                 Intent intent = new Intent("com.shixun.cms.REFRESH_DATA_BILL");
                 sendBroadcast(intent);
                 finish();
             }
             else{
                 Toast.makeText(AddBillActivity.this, "提交失败", Toast.LENGTH_SHORT).show();
             }
         }
     };

     private Handler handlerHouse = new Handler(Looper.myLooper()){
         @Override
         public void handleMessage(@NonNull Message msg) {
             Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
             if (result.getErrorCode() == Config.STATUS_OK) {
                 JSONArray jsonArray = (JSONArray) result.getData();
                 List<House> houses = jsonArray.toJavaList(House.class);
                 if (houses.size() == 0) {
                     Toast.makeText(AddBillActivity.this, "没有查询到与有关的房屋信息，无法添加账单信息", Toast.LENGTH_SHORT).show();
                     add_bill_submit.setEnabled(false);
                     return;
                 }

                 // 数据添加
                 houseArrayList = new ArrayList<>();
                 houseArrayList.addAll(houses);

                 add_bill_loading.setVisibility(View.GONE);
                 add_bill_content.setVisibility(View.VISIBLE);
             }
         }
     };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bill);
        Intent intent = getIntent();
        userInfo = (User) intent.getSerializableExtra("userInfo");
        managerInfo = Manager.loadLoginManagerInfo(this);
        initView();
        initHouseInfo();
    }

    /**
     * 加载 房屋信息
     */
    private void initHouseInfo() {
        OkHttpUtil.get(Config.URL_HOUSEINFO + userInfo.getUser_id(), handlerHouse);
    }

    private void initView() {
        add_bill_back = findViewById(R.id.add_bill_back);
        add_bill_back.setOnClickListener(view -> finish());

        add_bill_loading = findViewById(R.id.add_bill_loading);
        add_bill_content = findViewById(R.id.add_bill_content);

        add_bill_uid = findViewById(R.id.add_bill_uid);
        add_bill_uid.setText(userInfo.getUser_id());

        add_bill_username = findViewById(R.id.add_bill_username);
        add_bill_username.setText(userInfo.getUser_name());

        add_bill_phone = findViewById(R.id.add_bill_phone);
        add_bill_phone.setText(userInfo.getUser_tel());

        add_bill_houseid = findViewById(R.id.add_bill_houseid);
        add_bill_houseid.setOnClickListener(view -> chooseHouseID());

        add_bill_managerid = findViewById(R.id.add_bill_managerid);
        String mid = managerInfo.getString("manager_id","admin");
        add_bill_managerid.setText(mid);

        add_bill_age = findViewById(R.id.add_bill_age);
        add_bill_age.setText(String.valueOf(userInfo.getAge()));

        add_bill_chargeid = findViewById(R.id.add_bill_chargeid);
        add_bill_chargeid.setText(setBillID());

        add_bill_month = findViewById(R.id.add_bill_month);
        add_bill_water = findViewById(R.id.add_bill_water);
        add_bill_ele = findViewById(R.id.add_bill_ele);
        add_bill_gas = findViewById(R.id.add_bill_gas);
        add_bill_wuye = findViewById(R.id.add_bill_wuye);

        add_bill_submit = findViewById(R.id.add_bill_submit);
        add_bill_submit.setOnClickListener(view -> submit());

        add_bill_payday = findViewById(R.id.add_bill_payday);
        add_bill_payday.setOnClickListener(view -> chooseTime());

        // 加载房屋，隐藏下面
        add_bill_content.setVisibility(View.GONE);
    }

    private void submit() {
        add_bill_submit.setOnClickListener(view -> {
            String url = Config.URL_INSERT_CHARGE +
                    "cid=" + add_bill_chargeid.getText()+
                    "&hid=" + add_bill_houseid.getText() +
                    "&mid=" + add_bill_managerid.getText()+
                    "&uid=" + add_bill_uid.getText()+
                    "&mon=" + add_bill_month.getText().toString()+
                    "&wat=" + add_bill_water.getText().toString()+
                    "&elc=" + add_bill_ele.getText().toString()+
                    "&gas=" + add_bill_gas.getText().toString()+
                    "&wuy=" + add_bill_wuye.getText().toString()+
                    "&cst=" + 0 +
                    "&pdt=" + add_bill_payday.getText().toString();
            Log.e("URL------------->" , url);
            OkHttpUtil.get(url ,handler);

        });
    }

    private void chooseHouseID() {
        String[] houses = new String[houseArrayList.size()];
        for (int i = 0; i < houses.length; i++) {
            houses[i] = houseArrayList.get(i).getHouse_id();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setTitle("请选择");
        alertBuilder.setSingleChoiceItems(houses, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.e("CHOOSE_TYPE", houseArrayList.get(i).toString());
                String hid  = houseArrayList.get(i).getHouse_id();
                add_bill_houseid.setText(hid);
            }
        });

        alertBuilder.setPositiveButton("确定", (dialogInterface, i) -> alertDialog.dismiss());

        alertBuilder.setNegativeButton("取消", (dialogInterface, i) -> alertDialog.dismiss());

        alertDialog = alertBuilder.create();
        alertDialog.show();

    }

    private String setBillID() {
        return "Bill"+System.currentTimeMillis()/100;
    }

    private void chooseTime() {
        Calendar calendar = Calendar.getInstance();
        //create a datePickerDialog and then shoe it on your screen
        new DatePickerDialog(AddBillActivity.this,//binding the listener for your DatePickerDialog
                (view, year, month, dayOfMonth) -> {
                    String hopeTime;
                    month++;
                    Log.e("DatePicker","Year:" + year + " Month:" + month + " Day:" + dayOfMonth);
                    if (month>0 && month<10 && dayOfMonth>0&&dayOfMonth<10){
                        hopeTime= year +"-0"+ month +"-0"+dayOfMonth;
                        add_bill_payday.setText(hopeTime);
                        return;
                    }
                    else if (month>0 && month<10){
                        hopeTime= year +"-0"+ month +"-"+dayOfMonth;
                    }
                    else if (dayOfMonth>0&&dayOfMonth<10){
                        hopeTime= year +"-"+ month +"-0"+dayOfMonth;
                    }
                    else  hopeTime= year +"-"+ month +"-"+dayOfMonth;
                    add_bill_payday.setText(hopeTime);
                }
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}
