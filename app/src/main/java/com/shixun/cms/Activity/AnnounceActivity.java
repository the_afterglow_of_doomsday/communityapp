package com.shixun.cms.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Adapter.AnnounceAdapter;
import com.shixun.cms.Entity.Announce;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 查看公告页面
 */
public class AnnounceActivity extends AppCompatActivity {
    private RecyclerView rv_notice;
    private AnnounceAdapter announceAdapter;
    private LinearLayout loading;
    private List<Announce> data;
    private int userType;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            //网络请求失败
            if (msg.what == Config.STATUS_OK) {
                Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                JSONArray jsonArray = (JSONArray) result.getData();
                List<Announce> announces = jsonArray.toJavaList(Announce.class);
                data.addAll(announces);
                if (data.size() == 0){
                    Toast.makeText(AnnounceActivity.this, "数据请求失败，请重试", Toast.LENGTH_LONG).show();
                    loading.setVisibility(View.GONE);
                    return;
                }
                announceAdapter.notifyDataSetChanged();
                loading.setVisibility(View.GONE);
                rv_notice.setVisibility(View.VISIBLE);
            }
            else{
                Toast.makeText(AnnounceActivity.this, "数据请求失败，请刷新重试", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announce);
//        AndroidBarUtils.setTranslucent(this);
        initBroadcast();
        // 判断调用者：用户或管理员
        Intent intent = getIntent();
        userType = intent.getIntExtra("userType", Config.USER_TYPE_USER);

        loading = findViewById(R.id.announce_loading);
        // init RecycleView
        rv_notice = findViewById(R.id.rv_notice);
        rv_notice.setLayoutManager(new LinearLayoutManager(this));
        data = new ArrayList<>();
        // 绑定recycleview 与 adapter
        announceAdapter = new AnnounceAdapter(this, data, userType);
        rv_notice.setAdapter(announceAdapter);
        initData();
        ImageView back = findViewById(R.id.announce_back);
        back.setOnClickListener(v->finish());
    }

    private void initBroadcast() {
        // 建立IntentFilter对象
        IntentFilter intentFilter = new IntentFilter();
        // 想要监听什么广播,在addAction方法在添加
        intentFilter.addAction("com.shixun.cms.REFRESH_DATA_AN");
        AnnounceActivity.RefreshBroadcastReceiver loadNextPageReceiver = new RefreshBroadcastReceiver();
        // 注册广播接收器
        registerReceiver(loadNextPageReceiver, intentFilter);

    }

    private void initData() {
        OkHttpUtil.get(Config.URL_ANNOUNCE, handler);
    }

    private class RefreshBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("广播","r!!!!!!!!!!!!!!!!!!!!!!");
            rv_notice.setVisibility(View.GONE);
            loading.setVisibility(View.VISIBLE);
            data.clear();
            announceAdapter.notifyDataSetChanged();
            // 重新加载数据
            initData();
        }
    }
}
