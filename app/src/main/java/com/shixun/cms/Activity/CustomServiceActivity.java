package com.shixun.cms.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;
import com.shixun.cms.Util.QQUtil;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class CustomServiceActivity extends AppCompatActivity {
    private EditText custom_name, custom_email, custom_detail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_service);
//        AndroidBarUtils.setTranslucent(this);

        Button qqGroup = findViewById(R.id.custom_qqgroup);

        qqGroup.setOnClickListener(view -> {
            boolean result = new QQUtil(this).joinQQGroup(Config.REQUEST_QQGROUP_KEY);
            String resInfo = result ? "跳转成功" : "跳转失败";
            Toast.makeText(CustomServiceActivity.this, resInfo, Toast.LENGTH_SHORT).show();
        });
        custom_name = findViewById(R.id.custom_name);
        custom_email = findViewById(R.id.custom_email);
        custom_detail = findViewById(R.id.custom_detail);
        ImageView back = findViewById(R.id.custom_back);
        back.setOnClickListener(view -> finish());

        Button submit = findViewById(R.id.custom_submit);
        submit.setOnClickListener(v->{
            String name = custom_name.getText().toString();
            String email = custom_email.getText().toString();
            String detail = custom_detail.getText().toString();

            String resultStr = "# 反馈消息\n\n"
                    + "### 用户名\n\n" + name+ "\n\n"
                    + "### 邮箱\n\n" + email+ "\n\n"
                    + "###  详细\n\n" + detail+ "\n\n";

//            OkHttpUtil.sendOkHttpRequestByGetMethod(Config.URL_SERVERCHAN + "text=有新的反馈"+
//                            resultStr.substring(0,5)+"&" + "desp=" + resultStr,
//                    new Callback() {
//                        @Override
//                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
//                            Toast.makeText(CustomServiceActivity.this, "反馈失败", Toast.LENGTH_SHORT).show();
//                        }
//
//                        @Override
//                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
//                            if (response.code() == Config.STATUS_OK){
//                                Looper.prepare();
//                                Log.e("Log",response.body().string());
//                                Toast.makeText(CustomServiceActivity.this, "反馈成功", Toast.LENGTH_SHORT).show();
//                                Looper.loop();
//                            }
//                            else{
//                                Looper.prepare();
//                                Toast.makeText(CustomServiceActivity.this, "反馈失败", Toast.LENGTH_SHORT).show();
//                                Looper.loop();
//                            }
//                        }
//                    });
        });
    }

    /****************
     *
     * 发起添加群流程。群号：实训小组(1135537135) 的 key 为： AUvyTsgm9YBgG4ychKMtPk6CIiNOhvJB
     * 调用 joinQQGroup(AUvyTsgm9YBgG4ychKMtPk6CIiNOhvJB) 即可发起手Q客户端申请加群 实训小组(1135537135)
     *
     * @param key 由官网生成的key
     * @return 返回true表示呼起手Q成功，返回false表示呼起失败
     ******************/

}