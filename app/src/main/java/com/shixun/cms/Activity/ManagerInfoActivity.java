package com.shixun.cms.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.media.tv.TvContentRating;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.shixun.cms.Entity.Manager;
import com.shixun.cms.R;
import com.shixun.cms.Util.AndroidBarUtils;

import java.util.TimerTask;

public class ManagerInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_infoac);
//        AndroidBarUtils.setTranslucent(this);
        SharedPreferences managerInfo = Manager.loadLoginManagerInfo(this);
        String mid =managerInfo.getString("manager_id", "undefined");
        String maccount = managerInfo.getString("manager_account", "undefined");

        TextView tvID, tvAccount;
        tvID = findViewById(R.id.manager_info_id);
        tvID.setText(mid);

        tvAccount = findViewById(R.id.manager_info_account);
        tvAccount.setText(maccount);

        ImageView back = findViewById(R.id.manager_info_back);
        back.setOnClickListener(view -> finish());
    }
}
