package com.shixun.cms.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shixun.cms.Activity.HouseInfoActivity;
import com.shixun.cms.Activity.PropertyfeeActivity;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int USER_LAYOUT = 1;
    private static final int NO_MORE_TO_LOAD = -1;
    private Context context;
    private List<User> data;
    private int feeType;

    public UserListAdapter(Context context, List<User> data) {
        this.context = context;
        this.data = data;
    }

    public UserListAdapter(Context context, List<User> data, int feeType) {
        this.context = context;
        this.data = data;
        this.feeType = feeType;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size()){
            return NO_MORE_TO_LOAD;
        }
        return USER_LAYOUT;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item_layout, parent, false);
        View noMoreToLoad = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_more_to_load, parent,false);
        if (viewType == NO_MORE_TO_LOAD){
            return new UserListAdapter.RecyclerViewHolder(noMoreToLoad, NO_MORE_TO_LOAD);
        }
        else{
            return new UserListAdapter.RecyclerViewHolder(view, USER_LAYOUT);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == NO_MORE_TO_LOAD){
            final UserListAdapter.RecyclerViewHolder recyclerViewHolder = (UserListAdapter.RecyclerViewHolder) holder;
            recyclerViewHolder.Loading.setText("已经到底了");
        }
        else{
            final UserListAdapter.RecyclerViewHolder recyclerViewHolder = (UserListAdapter.RecyclerViewHolder) holder;
            User post  = data.get(position);
            recyclerViewHolder.UserName.setText(post.getUser_name());
            if (feeType == Config.FEE_TYPE){
                recyclerViewHolder.Desp.setText("点击进入物业费/水电费管理");
                recyclerViewHolder.itemView.setOnClickListener(v->{
                    Intent intent = new Intent(context, PropertyfeeActivity.class);
                    intent.putExtra("info",post);
                    intent.putExtra("feeType", Config.FEE_TYPE);
                    context.startActivity(intent);
                });
            }
            else{
                recyclerViewHolder.itemView.setOnClickListener(v->{
                    Intent intent = new Intent(context, HouseInfoActivity.class);
                    intent.putExtra("info",post);
                    intent.putExtra("userType", Config.USER_TYPE_MANAGER);
                    context.startActivity(intent);
                });
            }

        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 1;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView UserName, Loading, Desp;
        LinearLayout openDetail;

        RecyclerViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);

            if (viewType == USER_LAYOUT){
                UserName = itemView.findViewById(R.id.user_list_single_name);
                openDetail = itemView.findViewById(R.id.user_list_single_open_detail);
                Desp = itemView.findViewById(R.id.user_list_single_desp);
            }

            else if (viewType == NO_MORE_TO_LOAD){
                Loading = itemView.findViewById(R.id.foot_text_no_more);
            }
        }
    }
}
