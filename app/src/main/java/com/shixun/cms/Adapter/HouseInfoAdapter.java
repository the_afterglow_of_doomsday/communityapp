package com.shixun.cms.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shixun.cms.Activity.EditHouseInfoActivity;
import com.shixun.cms.Entity.Announce;
import com.shixun.cms.Entity.House;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;

import java.util.List;

public class HouseInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int NO_MORE_TO_LOAD = -1;
    private static final int NOTICE_LAYOUT = 1;
    private Context context;
    private List<House> data;
    private int userType;

    public HouseInfoAdapter(Context context, List<House> data, int userType) {
        this.context = context;
        this.data = data;
        this.userType = userType;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size()){
            return NO_MORE_TO_LOAD;
        }
        return NOTICE_LAYOUT;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.house_info_layout, parent, false);
        View noMoreToLoad = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_more_to_load, parent,false);
        if (viewType == NO_MORE_TO_LOAD){
            return new HouseInfoAdapter.RecyclerViewHolder(noMoreToLoad, NO_MORE_TO_LOAD);
        }
        else{
            return new HouseInfoAdapter.RecyclerViewHolder(view, NOTICE_LAYOUT);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == NO_MORE_TO_LOAD){
            final HouseInfoAdapter.RecyclerViewHolder recyclerViewHolder = (HouseInfoAdapter.RecyclerViewHolder) holder;
            recyclerViewHolder.Loading.setText("已经到底了");
        }
        else{
            final HouseInfoAdapter.RecyclerViewHolder recyclerViewHolder = (HouseInfoAdapter.RecyclerViewHolder) holder;
            House house  = data.get(position);
            String houseid = "No."+house.getHouse_id();
            String floor = "F"+house.getFloor();
            String area = house.getArea()+"m²";
            recyclerViewHolder.houseID.setText(houseid);
            recyclerViewHolder.houseFloor.setText(floor);
            recyclerViewHolder.houseArea.setText(area);
            recyclerViewHolder.houseAddress.setText(house.getAddress());
            if (userType == Config.USER_TYPE_MANAGER){
                recyclerViewHolder.itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(context, EditHouseInfoActivity.class);
                    intent.putExtra("info", house);
                    context.startActivity(intent);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 1;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView houseID, houseFloor, houseArea, houseAddress, Loading;

        RecyclerViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            if (viewType == NOTICE_LAYOUT){
                houseID = itemView.findViewById(R.id.houseinfo_hid);
                houseFloor = itemView.findViewById(R.id.houseinfo_floor_1);
                houseArea = itemView.findViewById(R.id.houseinfo_area);
                houseAddress = itemView.findViewById(R.id.houseinfo_address);

            }

            else if (viewType == NO_MORE_TO_LOAD){
                Loading = itemView.findViewById(R.id.foot_text_no_more);
            }
        }
    }
}
