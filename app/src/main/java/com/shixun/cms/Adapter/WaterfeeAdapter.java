package com.shixun.cms.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.shixun.cms.Activity.PayfeeActivity;
import com.shixun.cms.Activity.PaywaterfeeActivity;
import com.shixun.cms.Entity.Propertyfee;
import com.shixun.cms.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WaterfeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int NOTICE_LAYOUT = 1;
    private static final int NO_MORE_TO_LOAD = -1;
    private Context context;
    private List<Propertyfee> data;

    public WaterfeeAdapter(Context context, List<Propertyfee> data){

        this.context=context;
        this.data=data;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size()){
            return NO_MORE_TO_LOAD;
        }
        return NOTICE_LAYOUT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.water_fee_layout,parent,false);
        View noMoreToLoad=LayoutInflater.from(parent.getContext()).inflate(R.layout.no_more_to_load,parent,false);

        if (viewType == NO_MORE_TO_LOAD){
            return new WaterfeeAdapter.RecyclerViewHolder(noMoreToLoad, NO_MORE_TO_LOAD);
        }
        else{
            return new WaterfeeAdapter.RecyclerViewHolder(view, NOTICE_LAYOUT);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(getItemViewType(position)==NO_MORE_TO_LOAD){

            final WaterfeeAdapter.RecyclerViewHolder recyclerViewHolder=(WaterfeeAdapter.RecyclerViewHolder) holder;

            recyclerViewHolder.Loading.setText("已经到底了");
        }else {

            final WaterfeeAdapter.RecyclerViewHolder recyclerViewHolder=(WaterfeeAdapter.RecyclerViewHolder) holder;

            Propertyfee propertyfee=data.get(position);

            String listnumebr=propertyfee.getCharge_id();
            String money= String.valueOf(propertyfee.getWuye());
            String storey=propertyfee.getHouse_id();
            int situation= propertyfee.getCharge_status();
            Date date= propertyfee.getPaydate();
            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
            String date_result=formatter.format(date);
            String manageid=propertyfee.getManager_id();

            String gas= String.valueOf(propertyfee.getGas());
            String water= String.valueOf(propertyfee.getWater());
            String eletricty= String.valueOf(propertyfee.getElectric());

            float money_result=Float.parseFloat(gas)+Float.parseFloat(water)+Float.parseFloat(eletricty);
            String money_final= String.valueOf(money_result);


            recyclerViewHolder.waterfee_date.setText(date_result);
            recyclerViewHolder.waterfee_money.setText(money_final);
            if(situation==0){
                recyclerViewHolder.waterfee_situation.setText("未缴费");
                recyclerViewHolder.waterfee_situation.setTextColor(ContextCompat.getColor(context, R.color.red));
            }else{
                recyclerViewHolder.waterfee_situation.setText("已缴费");
                recyclerViewHolder.waterfee_situation.setTextColor(ContextCompat.getColor(context, R.color.light_blue));
            }
            recyclerViewHolder.waterfee_storey.setText(storey);
            recyclerViewHolder.waterfee_listnumber.setText(listnumebr);

            recyclerViewHolder.topaywaterfee.setOnClickListener(view -> {

                Intent intent = new Intent(view.getContext(), PaywaterfeeActivity.class);

                intent.putExtra("listnumber",listnumebr);
                intent.putExtra("money",money);
                intent.putExtra("storey",storey);
                intent.putExtra("situation",situation);
                intent.putExtra("date",date_result);
                intent.putExtra("managerid",manageid);

                intent.putExtra("gas",gas);
                intent.putExtra("water",water);
                intent.putExtra("electricity",eletricty);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                view.getContext().startActivity(intent);

            });

        }


    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView Loading,waterfee_listnumber,waterfee_money,waterfee_storey,waterfee_date,waterfee_situation;
        private Button topaywaterfee;

        public RecyclerViewHolder(View itemView, int viewType) {
            super(itemView);

            if(viewType==NO_MORE_TO_LOAD){

                Loading=itemView.findViewById(R.id.foot_text_no_more);

            }else{

                waterfee_listnumber=itemView.findViewById(R.id.waterfee_listnumber);
                waterfee_storey=itemView.findViewById(R.id.waterfee_storey);
                waterfee_situation=itemView.findViewById(R.id.waterfee_situation);
                waterfee_money=itemView.findViewById(R.id.waterfee_money);
                waterfee_date=itemView.findViewById(R.id.waterfee_date);
                topaywaterfee=itemView.findViewById(R.id.to_paywaterfee);

            }


        }
    }

}
