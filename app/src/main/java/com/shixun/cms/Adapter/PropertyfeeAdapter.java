package com.shixun.cms.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.shixun.cms.Activity.PayfeeActivity;
import com.shixun.cms.Activity.PropertyfeeActivity;
import com.shixun.cms.Entity.Propertyfee;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PropertyfeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int NOTICE_LAYOUT = 1;
    private static final int NO_MORE_TO_LOAD = -1;
    private Context context;
    private List<Propertyfee> data;
    private int feeType;

    public PropertyfeeAdapter(Context context, List<Propertyfee> data){

       this.context=context;
       this.data=data;

    }

    public PropertyfeeAdapter(Context context, List<Propertyfee> data ,int feeType){

        this.context=context;
        this.data=data;
        this.feeType = feeType;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size()){
            return NO_MORE_TO_LOAD;
        }
        return NOTICE_LAYOUT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_fee_layout,parent,false);
        View noMoreToLoad=LayoutInflater.from(parent.getContext()).inflate(R.layout.no_more_to_load,parent,false);

        if (viewType == NO_MORE_TO_LOAD){
            return new PropertyfeeAdapter.RecyclerViewHolder(noMoreToLoad, NO_MORE_TO_LOAD);
        }
        else{
            return new PropertyfeeAdapter.RecyclerViewHolder(view, NOTICE_LAYOUT);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

          if(getItemViewType(position)==NO_MORE_TO_LOAD){

              final PropertyfeeAdapter.RecyclerViewHolder recyclerViewHolder=(PropertyfeeAdapter.RecyclerViewHolder) holder;

              recyclerViewHolder.Loading.setText("已经到底了");
          }else {

              final PropertyfeeAdapter.RecyclerViewHolder recyclerViewHolder=(PropertyfeeAdapter.RecyclerViewHolder) holder;

              Propertyfee propertyfee=data.get(position);

              String listnumebr=propertyfee.getCharge_id();
              String money= String.valueOf(propertyfee.getWuye());
              String storey=propertyfee.getHouse_id();
              int situation= propertyfee.getCharge_status();
              Date date= propertyfee.getPaydate();
              SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
              String date_result=formatter.format(date);
              String houseid=propertyfee.getHouse_id();
              String manageid=propertyfee.getManager_id();

              recyclerViewHolder.fee_date.setText(date_result);
              recyclerViewHolder.fee_money.setText(money);
              if(situation==0){
                  recyclerViewHolder.fee_situation.setText("未缴费");
                  recyclerViewHolder.fee_situation.setTextColor(ContextCompat.getColor(context, R.color.red));
              }else{
                  recyclerViewHolder.fee_situation.setText("已缴费");
                  recyclerViewHolder.fee_situation.setTextColor(ContextCompat.getColor(context, R.color.light_blue));
              }
              recyclerViewHolder.fee_storey.setText(storey);
              recyclerViewHolder.fee_listnumber.setText(listnumebr);

              Intent intent = new Intent(context, PayfeeActivity.class);

              intent.putExtra("listnumber",listnumebr);
              intent.putExtra("money",money);
              intent.putExtra("storey",storey);
              intent.putExtra("situation",situation);
              intent.putExtra("date",date_result);
              intent.putExtra("houseid",houseid);
              intent.putExtra("managerid",manageid);

              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

              //管理员
              if (feeType == Config.FEE_TYPE){
                  intent.putExtra("feeType", Config.FEE_TYPE);
                  recyclerViewHolder.fee_situation.setVisibility(View.GONE);
                  recyclerViewHolder.fee_money.setVisibility(View.GONE);
                  recyclerViewHolder.topayfee.setText("查看详情");
              }
              recyclerViewHolder.topayfee.setOnClickListener(view -> {
                  context.startActivity(intent);
              });
          }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView Loading,fee_listnumber,fee_money,fee_storey,fee_date,fee_situation;
        private Button topayfee;

        public RecyclerViewHolder(View itemView, int viewType) {
            super(itemView);

            if(viewType==NO_MORE_TO_LOAD){

                Loading=itemView.findViewById(R.id.foot_text_no_more);

            }else{

                fee_listnumber=itemView.findViewById(R.id.fee_listnumber);
                fee_storey=itemView.findViewById(R.id.fee_storey);
                fee_situation=itemView.findViewById(R.id.fee_situation);
                fee_money=itemView.findViewById(R.id.fee_money);
                fee_date=itemView.findViewById(R.id.fee_date);
                topayfee=itemView.findViewById(R.id.to_payfee);

            }


        }
    }
}
