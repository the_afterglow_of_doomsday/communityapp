package com.shixun.cms.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.shixun.cms.Activity.DetailedRepairActivity;
import com.shixun.cms.Entity.Repair;
import com.shixun.cms.R;

import java.util.List;

public class RepairFormAdapter extends RecyclerView.Adapter {
    private static final int NO_MORE_TO_LOAD = -1;
    private static final int NOTICE_LAYOUT = 1;
    private Context context;
    private int userType;
    private List<Repair> data;


    public RepairFormAdapter(Context context, List<Repair> data, int userType) {
        this.context = context;
        this.data = data;
        this.userType = userType;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size()){
            return NO_MORE_TO_LOAD;
        }
        return NOTICE_LAYOUT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repair_form_layout, parent, false);
        View noMoreToLoad = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_more_to_load, parent,false);
        if (viewType == NO_MORE_TO_LOAD){
            return new RepairFormAdapter.RecyclerViewHolder(noMoreToLoad, NO_MORE_TO_LOAD);
        }
        else{
            return new RepairFormAdapter.RecyclerViewHolder(view, NOTICE_LAYOUT);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == NO_MORE_TO_LOAD){
            final RepairFormAdapter.RecyclerViewHolder recyclerViewHolder = (RepairFormAdapter.RecyclerViewHolder) holder;
            recyclerViewHolder.Loading.setText("已经到底了");
        }
        else{
            final RepairFormAdapter.RecyclerViewHolder recyclerViewHolder = (RepairFormAdapter.RecyclerViewHolder) holder;
            Repair repair  = data.get(position);
            String repairID = "No."+repair.getRepair_id();
            String repairType = repair.getRepair_type();
            String reportTime = repair.translateDateToString(repair.getReport_time());
            String status = repair.getRepait_status() == 0? "处理中,点击查看详情":"已处理,点击查看详情";
            recyclerViewHolder.repairID.setText(repairID);
            recyclerViewHolder.repairType.setText(repairType);
            recyclerViewHolder.reportTime.setText(reportTime);
            if (repair.getRepait_status() == 0){
                recyclerViewHolder.repaieStatus.setText(status);
                recyclerViewHolder.repaieStatus.setTextColor(ContextCompat.getColor(context,R.color.red));
            }
            else if (repair.getRepait_status() == 1){
                recyclerViewHolder.repaieStatus.setText(status);
                recyclerViewHolder.repaieStatus.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
            }

            recyclerViewHolder.itemView.setOnClickListener(v->{
                Intent intent = new Intent(context, DetailedRepairActivity.class);
                intent.putExtra("info", repair);
                intent.putExtra("userType", userType);
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 1;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView repairID, repairType, reportTime, repaieStatus, Loading;
        RecyclerViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            if (viewType == NOTICE_LAYOUT){
                repairID = itemView.findViewById(R.id.repair_form_rid);
                repairType = itemView.findViewById(R.id.repair_form_type);
                reportTime = itemView.findViewById(R.id.repair_form_time);
                repaieStatus = itemView.findViewById(R.id.repair_form_status);

            }

            else if (viewType == NO_MORE_TO_LOAD){
                Loading = itemView.findViewById(R.id.foot_text_no_more);
            }
        }
    }
}
