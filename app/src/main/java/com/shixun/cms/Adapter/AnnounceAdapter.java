package com.shixun.cms.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shixun.cms.Activity.DetailedAnnounceActivity;
import com.shixun.cms.Entity.Announce;
import com.shixun.cms.R;

import java.util.List;

public class AnnounceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int NOTICE_LAYOUT = 1;
    private static final int NO_MORE_TO_LOAD = -1;
    private Context context;
    private int userType;
    private List<Announce> data;
    @Override
    public int getItemViewType(int position) {
        if (position == data.size()){
            return NO_MORE_TO_LOAD;
        }
        return NOTICE_LAYOUT;
    }

    public AnnounceAdapter(Context context, List<Announce> data, int userType) {
        this.context = context;
        this.data = data;
        this.userType = userType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.announce_layout, parent, false);
        View noMoreToLoad = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_more_to_load, parent,false);
        if (viewType == NO_MORE_TO_LOAD){
            return new AnnounceAdapter.RecyclerViewHolder(noMoreToLoad, NO_MORE_TO_LOAD);
        }
        else{
            return new AnnounceAdapter.RecyclerViewHolder(view, NOTICE_LAYOUT);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == NO_MORE_TO_LOAD){
            final AnnounceAdapter.RecyclerViewHolder recyclerViewHolder = (AnnounceAdapter.RecyclerViewHolder) holder;
            recyclerViewHolder.Loading.setText("已经到底了");
        }
        else{
            final AnnounceAdapter.RecyclerViewHolder recyclerViewHolder = (AnnounceAdapter.RecyclerViewHolder) holder;
            Announce post  = data.get(position);
            recyclerViewHolder.tvNoticeTitle.setText("标题 "+post.getTitle());
            Log.e("NADAPTER","data set+ "+ recyclerViewHolder.tvNoticeTitle.getText());
            recyclerViewHolder.tvTitleCraeteTime.setText("发布时间 "+post.translateTimeStampToString());
            recyclerViewHolder.itemView.setOnClickListener(v->{
                Intent intent = new Intent(context, DetailedAnnounceActivity.class);
                intent.putExtra("info",post);
                intent.putExtra("userType", userType);
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 1;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView Loading, tvNoticeTitle, tvTitleCraeteTime;

        RecyclerViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);

            if (viewType == NOTICE_LAYOUT){
                tvNoticeTitle = itemView.findViewById(R.id.notice_title);
                tvTitleCraeteTime = itemView.findViewById(R.id.notice_create_time);
            }

            else if (viewType == NO_MORE_TO_LOAD){
                Loading = itemView.findViewById(R.id.foot_text_no_more);
            }
        }
    }
}
