package com.shixun.cms.Entity;

import android.content.Context;
import android.content.SharedPreferences;

public class Manager {

    private String manager_id;
    private String manager_account;
    private String manager_pwd;

    public Manager(String manager_id, String manager_account, String manager_pwd) {
        this.manager_id = manager_id;
        this.manager_account = manager_account;
        this.manager_pwd = manager_pwd;
    }

    public Manager(){

    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getmanager_account() {
        return manager_account;
    }

    public void setmanager_account(String manager_account) {
        this.manager_account = manager_account;
    }

    public String getmanager_pwd() {
        return manager_pwd;
    }

    public void setmanager_pwd(String manager_pwd) {
        this.manager_pwd = manager_pwd;
    }

    // 保存个人信息
    public static void saveLoginManagerInfo(Context context , Manager loginManagerInfo){
        SharedPreferences.Editor editor
                = context.getSharedPreferences("LoginManagerInfo", Context.MODE_PRIVATE)
                .edit();
        editor.putString("manager_id", loginManagerInfo.getManager_id());
        editor.putString("manager_account", loginManagerInfo.getmanager_account());
        editor.putString("manager_pwd",loginManagerInfo.getmanager_pwd());

        editor.apply();
    }

    public static SharedPreferences  loadLoginManagerInfo(Context context){
        return context.getSharedPreferences("LoginManagerInfo", Context.MODE_PRIVATE);
    }

    public static void deleteLoginInfo(Context context){
        SharedPreferences preferences = context.getSharedPreferences("LoginManagerInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}

