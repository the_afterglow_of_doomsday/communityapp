package com.shixun.cms.Entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 通知实体类
 */
public class Announce implements Serializable {
    public Announce() {
    }

    private String announce_id;

    public String getAnnounce_id() {
        return announce_id;
    }

    public void setAnnounce_id(String announce_id) {
        this.announce_id = announce_id;
    }

    public Announce(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getSend_date() {
        return send_date;
    }

    public void setSend_date(long send_date) {
        this.send_date = send_date;
    }

    public Announce(String announce_id, String manager_id, String title, String content, long send_date) {
        this.announce_id = announce_id;
        this.manager_id = manager_id;
        this.title = title;
        this.content = content;
        this.send_date = send_date;
    }

    public String translateTimeStampToString(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return simpleDateFormat.format(new Date(send_date));
    }

    private String manager_id;
    private String title;
    private String content;
    private long send_date;
}
