package com.shixun.cms.Entity;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.Serializable;

public class User implements Serializable {
    public User(String user_id, String user_account, String user_pwd, String user_name, String user_tel, int user_age) {
        this.user_id = user_id;
        this.user_account = user_account;
        this.user_pwd = user_pwd;
        this.user_name = user_name;
        this.user_tel = user_tel;
        this.age = user_age;
    }

    public User() {
    }

    private String user_id;
    private String user_account;
    private String user_pwd;
    private String user_name;
    private String user_tel;
    private int age;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_account() {
        return user_account;
    }

    public void setUser_account(String user_account) {
        this.user_account = user_account;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_tel() {
        return user_tel;
    }

    public void setUser_tel(String user_tel) {
        this.user_tel = user_tel;
    }

    public int getAge() {
        return age;
    }

    public void setUser_age(int user_age) {
        this.age = user_age;
    }


    // 保存个人信息
    public static void saveLoginUserInfo(Context context , User loginUserInfo){
        SharedPreferences.Editor editor
                = context.getSharedPreferences("LoginUserInfo", Context.MODE_PRIVATE)
                .edit();
        editor.putString("user_id", loginUserInfo.getUser_id());
        editor.putString("user_account", loginUserInfo.getUser_account());
        editor.putString("user_pwd",loginUserInfo.getUser_pwd());
        editor.putString("user_name", loginUserInfo.getUser_name());
        editor.putString("user_tel",loginUserInfo.getUser_tel());
        editor.putInt("age", loginUserInfo.getAge());

        editor.apply();
    }

    public static SharedPreferences  loadLoginUserInfo(Context context){
        return context.getSharedPreferences("LoginUserInfo", Context.MODE_PRIVATE);
    }

    public static void deleteLoginInfo(Context context){
        SharedPreferences preferences = context.getSharedPreferences("LoginUserInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}
