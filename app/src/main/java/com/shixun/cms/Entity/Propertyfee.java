package com.shixun.cms.Entity;

import java.util.Date;

public class Propertyfee {

    public Propertyfee(){

    }

    public Propertyfee(String charge_id, String house_id, String manager_id, String user_id, int month, float water,
                       float electric, float gas, float wuye, int charge_status, Date paydate) {
        this.charge_id = charge_id;
        this.house_id = house_id;
        this.manager_id = manager_id;
        this.user_id = user_id;
        this.month = month;
        this.water = water;
        this.electric = electric;
        this.gas = gas;
        this.wuye = wuye;
        this.charge_status = charge_status;
        this.paydate = paydate;
    }

    private String charge_id;
    private String house_id;
    private String manager_id;
    private String user_id;
    private int month;
    private float water;
    private float electric;
    private float gas;
    private float wuye;
    private int charge_status;
    private Date paydate;

    public String getCharge_id() {
        return charge_id;
    }

    public void setCharge_id(String charge_id) {
        this.charge_id = charge_id;
    }

    public String getHouse_id() {
        return house_id;
    }

    public void setHouse_id(String house_id) {
        this.house_id = house_id;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public float getWater() {
        return water;
    }

    public void setWater(float water) {
        this.water = water;
    }

    public float getElectric() {
        return electric;
    }

    public void setElectric(float electric) {
        this.electric = electric;
    }

    public float getGas() {
        return gas;
    }

    public void setGas(float gas) {
        this.gas = gas;
    }

    public float getWuye() {
        return wuye;
    }

    public void setWuye(float wuye) {
        this.wuye = wuye;
    }

    public int getCharge_status() {
        return charge_status;
    }

    public void setCharge_status(int charge_status) {
        this.charge_status = charge_status;
    }

    public Date getPaydate() {
        return paydate;
    }

    public void setPaydate(Date paydate) {
        this.paydate = paydate;
    }
}
