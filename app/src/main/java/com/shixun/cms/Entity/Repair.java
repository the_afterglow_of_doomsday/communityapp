package com.shixun.cms.Entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Repair implements Serializable {
    public Repair(String repair_id, String user_id, String manager_id, String repair_type, Date report_time,
                  Date repair_time, String principal, float repair_charge, Date pay_date, Date hope_time, int repair_status,String detail) {
        this.repair_id = repair_id;
        this.user_id = user_id;
        this.manager_id = manager_id;
        this.repair_type = repair_type;
        this.report_time = report_time;
        this.repair_time = repair_time;
        this.principal = principal;
        this.repair_charge = repair_charge;
        this.pay_date  = pay_date;
        this.hope_time = hope_time;
        this.repait_status = repair_status;
        this.detail = detail;

    }

    public Repair() {
    }

    private String repair_id;
    private String user_id;
    private String manager_id;
    private String repair_type;
    private Date report_time;
    private Date repair_time;
    private String principal;
    private float repair_charge;
    private Date pay_date;
    private Date hope_time;
    private int repait_status;
    private String detail;

    public String getRepair_id() {
        return repair_id;
    }

    public void setRepair_id(String repair_id) {
        this.repair_id = repair_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getRepair_type() {
        return repair_type;
    }

    public void setRepair_type(String repair_type) {
        this.repair_type = repair_type;
    }

    public Date getReport_time() {
        return report_time;
    }

    public void setReport_time(Date report_time) {
        this.report_time = report_time;
    }

    public Date getRepair_time() {
        return repair_time;
    }

    public void setRepair_time(Date repair_age) {
        this.repair_time = repair_age;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public float getRepair_charge() {
        return repair_charge;
    }

    public void setRepair_charge(float repair_charge) {
        this.repair_charge = repair_charge;
    }

    public Date getPay_date() {
        return pay_date;
    }

    public void setPay_date(Date pay_date) {
        this.pay_date = pay_date;
    }

    public Date getHope_time() {
        return hope_time;
    }

    public void setHope_time(Date hope_time) {
        this.hope_time = hope_time;
    }

    public int getRepait_status() {
        return repait_status;
    }

    public void setRepait_status(int repait_status) {
        this.repait_status = repait_status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String translateDateToString(Date date){
        if (date == null){
            return "";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }
}
