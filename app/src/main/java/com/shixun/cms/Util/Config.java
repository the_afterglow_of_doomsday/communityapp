package com.shixun.cms.Util;

import java.util.ArrayList;

public class Config {
    public static final String FILE_PREFERENCE = "preference";

    public static final String SETTTING_REMEMBER = "setting_remember";
    public static final String SETTTING_AUTO_LOGIN = "setting_auto_login";

    public static final String DATA_USERNAME = "data_username";
    public static final String DATA_PASSWORD = "data_password";

    //请求方式常量
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";

    //请求状态码常量
    public static final int STATUS_ERROR = -1;
    public static final int STATUS_OK = 200;
    public static final int STATUS_FAILURE = 0;
    public static final int STATUS_SUCCESS = 200;

    //请求参数名常量
    public static final String REQUEST_PARAMETER_USERNAME = "uaccount";
    public static final String REQUEST_PARAMETER_PASSWORD = "upassword";
    public static final String REQUEST_PARAMETER_NICKNAME = "unickname";
    public static final String REQUEST_PARAMETER_SEX = "usex";
    public static final String REQUEST_PARAMETER_METHOD = "method";
    public static final String REQUEST_PARAMETER_PAGE_CURRENT = "page_current";
    public static final String REQUEST_PARAMETER_PAGE_SIZE = "page_size";
    public static final String REQUEST_PARAMETER_SEARCH_WORD = "search_word";
    public static final String REQUEST_PARAMETER_MOVIE_ID = "mid";

    //请求参数值常量
    public static final String REQUEST_VALUE_METHOD_LIST_BY_PAGE = "list_by_page";
    public static final String REQUEST_VALUE_METHOD_LIST = "list";
    public static final String REQUEST_VALUE_METHOD_ONE = "one";
    public static final String REQUEST_VALUE_METHOD_SEARCH = "search";
    public static final int REQUEST_VALUE_PAGE_SIZE = 10;

    public static final String REQUEST_QQGROUP_KEY = "AUvyTsgm9YBgG4ychKMtPk6CIiNOhvJB";
    public static final String URL_SERVERCHAN = "https://sc.ftqq.com/SCU91936Ta39976e0c70dd9fdd9085eaae5d779095e82cb24cf95d.send?";

    //请求地址常量
    public static final String URL_LOGIN = "http://118.89.234.99:8080/xms/login";
    public static final String URL_ADMIN = "http://118.89.234.99:8080/xms/manager";
    public static final String URL_REPAIRE =  "http://118.89.234.99:8080/xms/insertrepair?";
    public static final String URL_REGISTER = "http://169.254.156.213:8080/ServletTest/register";
    public static final String URL_MOVIE = "http://169.254.156.213:8080/ServletTest/movie";


    public static final String URL_ANNOUNCE = "http://118.89.234.99:8080/xms/announce";
    public static final String URL_HOUSEINFO = "http://118.89.234.99:8080/xms/house?uid=";
    public static final String URL_REPAIR_ME = "http://118.89.234.99:8080/xms/queryrepair?uid=";
    public static final String URL_PROPERTYFEEINFO="http://118.89.234.99:8080/xms/querycharge?uid=";
    public static final String URL_SUCCESSPAYFEE="http://118.89.234.99:8080/xms/updatecharge";
    public static final String URL_CHECKUSERACCOUNT="http://118.89.234.99:8080/xms/queryuser";

    //banner
    public static final String[] BANNER_PICS = {"https://s1.ax1x.com/2020/07/27/aFlrB6.jpg",
                                                "https://s1.ax1x.com/2020/07/27/aFlR9H.jpg",
                                                "https://s1.ax1x.com/2020/07/27/aFlO3j.jpg"};

    public static final String URL_UPDATE_USERINFO = "http://118.89.234.99:8080/xms/updateuser?";
    public static final int CHANGE_PIC = 6324;
    public static final String SETTTING_AUTO_LOGIN_MANAGER = "auto_login_manager";

    public static final String URL_NEW_ANNOUNCE = "http://118.89.234.99:8080/xms/insertAnnounce?";

    public static final int UNDEFINED = -1;
    public static final int USER_TYPE_USER = 1;
    public static final int USER_TYPE_MANAGER = 2;

    public static final String URL_UPDATE_REPAIR = "http://118.89.234.99:8080/xms/updaterepair?";
    public static final String URL_UPDATE_ANNOUNCE = "http://118.89.234.99:8080/xms/updateannounce?";
    public static final String URL_DELETE_ANNOUNCE = "http://118.89.234.99:8080/xms/deleteannounce?";
    public static final String URL_UPDATE_HOUSEINFO = "http://118.89.234.99:8080/xms/updatehouse?";
    public static final String URL_INSERT_HOUSEINFO = "http://118.89.234.99:8080/xms/inserthouse?";
    public static final String URL_QUERY_ALL_USER = "http://118.89.234.99:8080/xms/queryall";
    public static final int FEE_TYPE = 1;

    public static final String URL_INSERT_CHARGE = "http://118.89.234.99:8080/xms/insertcharge?";
}
