package com.shixun.cms.Fragment;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shixun.cms.Activity.AnnounceActivity;
import com.shixun.cms.Activity.FeedbackActivity;
import com.shixun.cms.Activity.PropertyfeeActivity;
import com.shixun.cms.Activity.RepairActivity;
import com.shixun.cms.Activity.WaterfeeActivity;
import com.shixun.cms.Adapter.AnnounceAdapter;
import com.shixun.cms.Adapter.LooperPagerAdapter;
import com.shixun.cms.Entity.Announce;
import com.shixun.cms.Entity.Result;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.OkHttpUtil;
import com.shixun.cms.Util.QQUtil;
import com.shixun.cms.Util.SizeUtil;

import java.util.ArrayList;
import java.util.List;

public class FragmentOne extends Fragment {
    private AnnounceAdapter announceAdapter;
    private RecyclerView rv_notice;
    private List<Announce> announceList;
    private LinearLayout layout_loading, pointer;
    private Button bt_torepair, bt_toannounce, bt_human_service, bt_suggestion,bt_topropertyfee,bt_towaterfee;
    private ViewPager bannerViewPager;
    private Handler handler = new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            //网络请求失败
            if (msg.what == Config.STATUS_OK) {
                initRecycleView();
                Result result = JSONObject.parseObject(msg.obj.toString(), Result.class);
                JSONArray jsonArray = (JSONArray) result.getData();
                List<Announce> announces = jsonArray.toJavaList(Announce.class);
                announceList.addAll(announces);
                if (announceList.size() == 0){
                    Toast.makeText(getContext(), "数据请求失败，请重试", Toast.LENGTH_LONG).show();
                    layout_loading.setVisibility(View.GONE);
                    return;
                }
                announceAdapter.notifyDataSetChanged();
                layout_loading.setVisibility(View.GONE);
                rv_notice.setVisibility(View.VISIBLE);
            }
            else if(msg.what == Config.CHANGE_PIC){
                int index = (bannerViewPager.getCurrentItem()+1) % Config.BANNER_PICS.length;
                bannerViewPager.setCurrentItem(index);
                handler.sendEmptyMessageDelayed(Config.CHANGE_PIC,2000);
            }
            else{
                Toast.makeText(getContext(), "数据请求失败，请刷新重试", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler.sendEmptyMessageDelayed(Config.CHANGE_PIC,2000);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_one, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        announceList = new ArrayList<>();
        // 获取view对象的方法
        // Button button = (Button)view.findViewById(R.id.xxx);
        layout_loading = view.findViewById(R.id.fragment_one_loading);
        bt_torepair = view.findViewById(R.id.bt_torepair);
        bt_human_service = view.findViewById(R.id.bt_human_service);
        bt_human_service.setOnClickListener(v->{
//            Intent intent = new Intent(getActivity(), CustomServiceActivity.class);
//            startActivity(intent);
           AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext())
                   .setTitle("提示")
                   .setMessage("是否添加客服QQ群")
                   .setCancelable(true)
                   .setPositiveButton("是", (dialogInterface, i) -> {
                       boolean b = new QQUtil(getContext()).joinQQGroup(Config.REQUEST_QQGROUP_KEY);
                       if (!b){
                           Toast.makeText(getContext(),"拉起QQ失败", Toast.LENGTH_SHORT).show();
                       }
                   })
                   .setNegativeButton("否", (dialogInterface, i) -> dialogInterface.dismiss());
           alertBuilder.show();

        });
        bt_torepair.setOnClickListener(v->{
            Intent intent = new Intent(getActivity(), RepairActivity.class);
            startActivity(intent);
        });

        bt_toannounce = view.findViewById(R.id.bt_toannounce);
        bt_toannounce.setOnClickListener(v->{
            Intent intent = new Intent(getActivity(), AnnounceActivity.class);
            startActivity(intent);
        });
        bt_suggestion = view.findViewById(R.id.bt_suggestion);
        bt_suggestion.setOnClickListener(v->{
            Intent intent = new Intent(getActivity(), FeedbackActivity.class);
            startActivity(intent);
        });

        bt_topropertyfee=view.findViewById(R.id.bt_community_fee);
        bt_topropertyfee.setOnClickListener(view1 -> {
            Intent intent=new Intent(getActivity(), PropertyfeeActivity.class);
            startActivity(intent);
        });

        bt_towaterfee=view.findViewById(R.id.bt_waterfee);
        bt_towaterfee.setOnClickListener(view12 -> {
            Intent intent=new Intent(getActivity(), WaterfeeActivity.class);
            startActivity(intent);
        });

        pointer = view.findViewById(R.id.banner_pointer);
        bannerViewPager = view.findViewById(R.id.fragment1_viewpager);
        initViewPager();
        // 轮播图的点指示器
        initPointer();
        initData();


    }

    // 处理轮播图点指示器相关
    private void initPointer() {
        // 设置中间点
        bannerViewPager.setCurrentItem(0);

        for (int i = 0; i < Config.BANNER_PICS.length; i++) {
            View singlePoint = new View(getContext());
            int px = SizeUtil.dp2px(getContext(), 8);
            LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(px,px);

            params.leftMargin = SizeUtil.dp2px(getContext(), 5);
            params.rightMargin = SizeUtil.dp2px(getContext(), 5);

            singlePoint.setLayoutParams(params);
            pointer.addView(singlePoint);
        }
    }

    // 轮播图相关
    private void initViewPager() {
        // 创建适配器
        LooperPagerAdapter adapter = new LooperPagerAdapter();
        bannerViewPager.setOffscreenPageLimit(Config.BANNER_PICS.length);
        // 设置适配器
        bannerViewPager.setAdapter(adapter);
        bannerViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int targetPostion = position % Config.BANNER_PICS.length;
                // 切换指示器
                updateIndicator(targetPostion);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        // 自动切换

    }

    /**
     * 切换指示器 小圆点
     * @param pos 位置
     */
    private void updateIndicator(int pos) {
        for (int i = 0; i < pointer.getChildCount(); i++) {
            View pointerChild = pointer.getChildAt(i);
            if (i == pos){
                pointerChild.setBackgroundResource(R.drawable.shape_point_selected);
            }else{
                pointerChild.setBackgroundResource(R.drawable.shape_point_normal);
            }
        }
    }

    private void initRecycleView(){
        rv_notice = getActivity().findViewById(R.id.rv_notice);
        rv_notice.setLayoutManager(new LinearLayoutManager(getContext()));
        // 绑定recycleview 与 adapter
        announceAdapter = new AnnounceAdapter(getContext(), announceList, Config.USER_TYPE_USER);
        rv_notice.setAdapter(announceAdapter);
        rv_notice.setVisibility(View.GONE);
    }

    private void initData() {
        OkHttpUtil.get(Config.URL_ANNOUNCE, handler);
    }
}
