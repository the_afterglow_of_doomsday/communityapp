package com.shixun.cms.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.shixun.cms.Activity.HouseInfoActivity;
import com.shixun.cms.Activity.LoginActivity;
import com.shixun.cms.Activity.RepairListActivity;
import com.shixun.cms.Activity.SettingActivity;
import com.shixun.cms.Activity.SupportActivity;
import com.shixun.cms.Activity.UserInfoActivity;
import com.shixun.cms.Adapter.RepairFormAdapter;
import com.shixun.cms.Entity.User;
import com.shixun.cms.R;
import com.shixun.cms.Util.Config;
import com.shixun.cms.Util.PreferenceUtil;

public class FragmentThree extends Fragment {
    private LinearLayout setting;
    private LinearLayout userInfo, houseInfo, repairForm;
    private TextView wuyeInfo, tv_userName;
    private Button logout;
    private SharedPreferences preferences;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_three, container, false);
        preferences = User.loadLoginUserInfo(getContext());
        initView(view);
        initAction();
        return view;
    }

    private void initAction() {
        setting.setOnClickListener(v->getContext().startActivity(new Intent(getContext(), SettingActivity.class)));
        userInfo.setOnClickListener(v->getContext().startActivity(new Intent(getContext(), UserInfoActivity.class)));
        wuyeInfo.setOnClickListener(v->{
            Intent intent = new Intent(getActivity(), SupportActivity.class);
            intent.putExtra("title",true);
            startActivity(intent);
        });
        logout.setOnClickListener(v->{
            PreferenceUtil.putBoolean(getActivity(), Config.SETTTING_REMEMBER, false);
            PreferenceUtil.putBoolean(getActivity(), Config.SETTTING_AUTO_LOGIN, false);

            User.deleteLoginInfo(getActivity());
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        });

        houseInfo.setOnClickListener(v->{
            getContext().startActivity(new Intent(getContext(), HouseInfoActivity.class));
        });

        repairForm.setOnClickListener(view -> {
            getContext().startActivity(new Intent(getContext(), RepairListActivity.class));
        });
    }

    private void initView(View view) {
        userInfo = view.findViewById(R.id.fragment3_user_info);
        setting = view.findViewById(R.id.fragment3_setting);
        wuyeInfo = view.findViewById(R.id.fragment3_wuye);
        logout = view.findViewById(R.id.fragment3_logout);
        houseInfo = view.findViewById(R.id.fragment3_house);
        String userName = preferences.getString("user_name", "undefined");
        tv_userName = view.findViewById(R.id.fragment3_username);
        tv_userName.setText(userName);
        repairForm = view.findViewById(R.id.fragment3_repair_form);
    }
}
